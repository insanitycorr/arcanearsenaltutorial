﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void RenderNormalTex::Start()
extern void RenderNormalTex_Start_m0F0304CF50704B43665FF8A08E5F624FB7FB6C89 ();
// 0x00000002 System.Void RenderNormalTex::.ctor()
extern void RenderNormalTex__ctor_m29702AD449C586425B292EBCE0AE2F17320F8FB1 ();
// 0x00000003 System.Void RobertsCrossOutline::.ctor()
extern void RobertsCrossOutline__ctor_m349072DCDF1624B77573A6250CED4D0352394733 ();
// 0x00000004 System.Void RobertsCrossOutlineRenderer::Render(UnityEngine.Rendering.PostProcessing.PostProcessRenderContext)
extern void RobertsCrossOutlineRenderer_Render_mE969B77DAE3A3A1F72657EC29853C031FC5700C5 ();
// 0x00000005 System.Void RobertsCrossOutlineRenderer::.ctor()
extern void RobertsCrossOutlineRenderer__ctor_m5061522BFE0C08460C1DB3AF8F163DB5995BB540 ();
// 0x00000006 System.Void GunHandler::Start()
extern void GunHandler_Start_m2822DA132E5DBE81DC4DA3BDC11D82E81E2544F9 ();
// 0x00000007 System.Void GunHandler::Update()
extern void GunHandler_Update_mBEEEDE8ECF48CD4D4E08863BF86A8723F82EF4B0 ();
// 0x00000008 System.Void GunHandler::Fire()
extern void GunHandler_Fire_m5ED4DD0E042957D1F0B6BCEE5398900F7677D7C3 ();
// 0x00000009 System.Void GunHandler::updateAmmoHud()
extern void GunHandler_updateAmmoHud_mAEB33FE32C04364E783BA08C76E7F493968ADC4F ();
// 0x0000000A System.Void GunHandler::dryFire()
extern void GunHandler_dryFire_mB50AE26BF459A7DB9A22138C8E51D502E847FC14 ();
// 0x0000000B System.Void GunHandler::reload()
extern void GunHandler_reload_m7560C136992B9E49E24AAEAF3A8A60A76C2F1DEF ();
// 0x0000000C System.Void GunHandler::updateWeapon()
extern void GunHandler_updateWeapon_m73EB8B2F18F12C4E346D0430CE6241027FE405F5 ();
// 0x0000000D System.Void GunHandler::.ctor()
extern void GunHandler__ctor_m5B55A71AAF92AE08EF7B746C3A38EB8AF19FAA77 ();
// 0x0000000E System.Void Guns::.ctor()
extern void Guns__ctor_mAD180587AA05F9041BAB28DBB7D08A3425F27EF8 ();
// 0x0000000F System.Void GunSwap::Update()
extern void GunSwap_Update_mA362B35D4CA4DE71790E5E6C07AB73159CBB252D ();
// 0x00000010 System.Void GunSwap::.ctor()
extern void GunSwap__ctor_m57BFBE216176EA2D13DDBAC0012247974ADB5B87 ();
// 0x00000011 System.Void DummyDie::Start()
extern void DummyDie_Start_mB454721F4386BF260E4E20D51081E6DD24958AF6 ();
// 0x00000012 System.Void DummyDie::Update()
extern void DummyDie_Update_m78D8EE66B78F630F055E7253A96210FBF8D2190E ();
// 0x00000013 System.Void DummyDie::.ctor()
extern void DummyDie__ctor_m33050536DDB42656AF4D7E2F3CC2A4FBF00E4185 ();
// 0x00000014 System.Void EnemySpanwer::Start()
extern void EnemySpanwer_Start_m2A09ADDBC5640C1BCFE2E31F101730AA51DB4F73 ();
// 0x00000015 System.Void EnemySpanwer::Update()
extern void EnemySpanwer_Update_m83FAFBB19B6DEC949EF8CA6E6AD0243993DE6DDD ();
// 0x00000016 System.Void EnemySpanwer::.ctor()
extern void EnemySpanwer__ctor_m06F5873FF792FF12D3A44567DB28B92A2B146440 ();
// 0x00000017 System.Void Foot::Update()
extern void Foot_Update_m59F7B517606C811F33BE058FDD686F2C4796C6B5 ();
// 0x00000018 System.Void Foot::Step(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Foot_Step_mD0D293A76FBD362B823F73017B5DBF3D5B3F5E18 ();
// 0x00000019 System.Void Foot::Restart()
extern void Foot_Restart_m94971F8565D4CA88C16E1216483644256610467D ();
// 0x0000001A System.Void Foot::.ctor()
extern void Foot__ctor_mAC0D26757825E48575EAD3399BDB12CA7B6F2C7D ();
// 0x0000001B System.Void HoverBall::Start()
extern void HoverBall_Start_m1C07FA164F357E857DF4934F9AEFBB951F6C1D5A ();
// 0x0000001C System.Void HoverBall::Update()
extern void HoverBall_Update_m6BCD403C77D2AA1BDA9ACB9C59FBC96E7722EAA5 ();
// 0x0000001D System.Void HoverBall::FixedUpdate()
extern void HoverBall_FixedUpdate_m0C2502A1A70DC4BFAE8F8249BF18C46C6AFA1459 ();
// 0x0000001E System.Void HoverBall::OnDrawGizmos()
extern void HoverBall_OnDrawGizmos_m6DFA2F5BF4BA219CE21A00BB42EBA64ECE15CA5A ();
// 0x0000001F System.Void HoverBall::.ctor()
extern void HoverBall__ctor_m9B16105ACDEFFA8C7FFBE39B9E4621A58CAC0B48 ();
// 0x00000020 System.Void NavmeshAI::Start()
extern void NavmeshAI_Start_m086124CDD4F460FEADF6B4EA8F842590A28EF908 ();
// 0x00000021 System.Void NavmeshAI::Update()
extern void NavmeshAI_Update_m370E2324FF79360B691942027E79BF38F828AF13 ();
// 0x00000022 System.Void NavmeshAI::.ctor()
extern void NavmeshAI__ctor_mCCA744D57B9DB412CB204CDC25F293323712F2B5 ();
// 0x00000023 System.Void SpawnSelfOnDeactivate::OnApplicationQuit()
extern void SpawnSelfOnDeactivate_OnApplicationQuit_m6171CFAA96C5D19402555E79D2387933ED1DF511 ();
// 0x00000024 System.Void SpawnSelfOnDeactivate::OnDisable()
extern void SpawnSelfOnDeactivate_OnDisable_m376406D17232981AC218D130C54C8D50F12A192C ();
// 0x00000025 System.Void SpawnSelfOnDeactivate::.ctor()
extern void SpawnSelfOnDeactivate__ctor_m535A79F5898A36D2CD23B6884B94413232A29AE1 ();
// 0x00000026 System.Void SpawnSelfOnDie::OnApplicationQuit()
extern void SpawnSelfOnDie_OnApplicationQuit_mC5A58B4A96BB5E737A365AA48B3344ED935F7B33 ();
// 0x00000027 System.Void SpawnSelfOnDie::OnDestroy()
extern void SpawnSelfOnDie_OnDestroy_mF9E8944D8878FB87E41C25969B8D511C86EB5A2D ();
// 0x00000028 System.Void SpawnSelfOnDie::.ctor()
extern void SpawnSelfOnDie__ctor_m4039CFB0811DE19CE2656860C266F1D9912D9553 ();
// 0x00000029 System.Void ChainLightning::Awake()
extern void ChainLightning_Awake_m6E0DF75604A51D7BC17F05B676275DA2EE626BB3 ();
// 0x0000002A System.Void ChainLightning::Update()
extern void ChainLightning_Update_mDE0040864F4CBD840A778AA1E15D4CD84048A0E2 ();
// 0x0000002B System.Void ChainLightning::OnTriggerEnter(UnityEngine.Collider)
extern void ChainLightning_OnTriggerEnter_m4845910BB048BE756019CF01B0E1AED12CF90D06 ();
// 0x0000002C System.Void ChainLightning::.ctor()
extern void ChainLightning__ctor_m8D6AE9E7E97EB3D430E9FE3C7C3FB542E1F09B4C ();
// 0x0000002D System.Void ExplosionLogic::Awake()
extern void ExplosionLogic_Awake_mC475951C99AF5674DB8661D579017E34469F3689 ();
// 0x0000002E System.Void ExplosionLogic::Update()
extern void ExplosionLogic_Update_m3E1AFAB0B30838F02A8B8E9EDA82BDB9F6C0B2E6 ();
// 0x0000002F System.Void ExplosionLogic::OnTriggerEnter(UnityEngine.Collider)
extern void ExplosionLogic_OnTriggerEnter_m4654E5915B04EF9B07DF4809F62649DFE3A1D8BB ();
// 0x00000030 System.Void ExplosionLogic::OnDrawGizmos()
extern void ExplosionLogic_OnDrawGizmos_m6DE525778043695E684BC7CAEFD8C8270ECB110D ();
// 0x00000031 System.Void ExplosionLogic::.ctor()
extern void ExplosionLogic__ctor_mE44FB9A9A4526DE9286F38EFB60D501CBDF6E9DE ();
// 0x00000032 System.Void Fireball::Awake()
extern void Fireball_Awake_mCF516BFAF355DBD2AFE03799764774B03862B711 ();
// 0x00000033 System.Void Fireball::OnCollisionEnter(UnityEngine.Collision)
extern void Fireball_OnCollisionEnter_m560D6607D33B367E6ABC11C38B0CECC9E37F779E ();
// 0x00000034 System.Void Fireball::.ctor()
extern void Fireball__ctor_mC7C51E9F63A9C13D675C35E310B04E404FC68C58 ();
// 0x00000035 System.Void KillAfterTime::Awake()
extern void KillAfterTime_Awake_mEE7E0EE26AE4484E11BFC17614B5FB0683D33BDD ();
// 0x00000036 System.Void KillAfterTime::Update()
extern void KillAfterTime_Update_mCB5157EBFCAD785A6E9667F6DE6F1952E7F74897 ();
// 0x00000037 System.Void KillAfterTime::.ctor()
extern void KillAfterTime__ctor_m0C677F602EDD44351E7AD9126A91C76CA8E4E980 ();
// 0x00000038 System.Void MagicMissile::Start()
extern void MagicMissile_Start_mD8FEEB465DA4EB30F5B89EDC835DE3A665BB0471 ();
// 0x00000039 System.Void MagicMissile::Update()
extern void MagicMissile_Update_m0B35400AB6AA3FF63820EFC63B0B6BCCC9ADC77F ();
// 0x0000003A System.Void MagicMissile::FixedUpdate()
extern void MagicMissile_FixedUpdate_mCB4E5057CC417059767634ADCF413EFDAC05362D ();
// 0x0000003B System.Void MagicMissile::OnCollisionEnter(UnityEngine.Collision)
extern void MagicMissile_OnCollisionEnter_m74CE185B4808211F4CD1CC8E3A6A2D521551B4DE ();
// 0x0000003C System.Void MagicMissile::.ctor()
extern void MagicMissile__ctor_m06EB55A88861F31E3C3ACAE21A649160D39BE704 ();
// 0x0000003D System.Void Spell::.ctor()
extern void Spell__ctor_m43A4A689F2E85395D858EFE1181B98DBC19EC41B ();
// 0x0000003E System.Void SpellController::Start()
extern void SpellController_Start_mD92203C47CC08FE909D69B02FAC9B30CE324E779 ();
// 0x0000003F System.Void SpellController::Update()
extern void SpellController_Update_m98BFED179FBE125F09E282DCB0466C2D89F66861 ();
// 0x00000040 System.Void SpellController::.ctor()
extern void SpellController__ctor_mB3582C212C2BD439304EC77ECE88C2A1D349FA7A ();
// 0x00000041 System.Void SpellData::.ctor()
extern void SpellData__ctor_m0FB708800B8514D090D242470813647F9928A64C ();
// 0x00000042 System.Void Shootable::Start()
extern void Shootable_Start_m77E171A94C2CE67AB1E72E046AFF59F70656E630 ();
// 0x00000043 System.Void Shootable::TakeDamage(System.Single)
extern void Shootable_TakeDamage_mBD5C79B687356B2CA5510C019C7CE3271BE724B6 ();
// 0x00000044 System.Void Shootable::ResetHealth()
extern void Shootable_ResetHealth_mC25DBBF80946AC5BAE42C51B87666669663A6D87 ();
// 0x00000045 System.Void Shootable::Die()
extern void Shootable_Die_m65292A58A36966E02E9B4F1AFCD27EDBB68F0AF0 ();
// 0x00000046 System.Void Shootable::.ctor()
extern void Shootable__ctor_mBEF8970F3132EBD8498449742E21F6A0DBAF544E ();
// 0x00000047 System.Void ShootablePool::Die()
extern void ShootablePool_Die_m700296F2E5E5BEFB8E79FFF10E698837DD2B1C8A ();
// 0x00000048 System.Void ShootablePool::.ctor()
extern void ShootablePool__ctor_m183B3460651A5BFEA0D0338290DEE77CDFA03520 ();
// 0x00000049 System.Void ObjectPoolManager::.ctor()
extern void ObjectPoolManager__ctor_mD953D052C283DB2A5B1BD959FE9774DB3DE02520 ();
// 0x0000004A System.Void ObjectPoolManager::InitPool(UnityEngine.GameObject,System.String,System.Int32)
extern void ObjectPoolManager_InitPool_m7F2DDE9647DEBD656E1BC80AE11888C48D093921 ();
// 0x0000004B System.Collections.Generic.Queue`1<UnityEngine.GameObject> ObjectPoolManager::GetPool(System.String)
extern void ObjectPoolManager_GetPool_mB954C06D49579A6AA5CC9886763802C071D3C0E5 ();
// 0x0000004C UnityEngine.GameObject ObjectPoolManager::GetObject(System.String)
extern void ObjectPoolManager_GetObject_m5DCFCB7894165B82392FF9A9BC13416F2CF06FFE ();
// 0x0000004D System.Void ObjectPoolManager::PoolObject(UnityEngine.GameObject,System.String)
extern void ObjectPoolManager_PoolObject_mE6A2332012CDBFD60729BF6058FFC1F531AA1FD3 ();
// 0x0000004E System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Queue`1<UnityEngine.GameObject>>> ObjectPoolManager::FindPools(System.String)
extern void ObjectPoolManager_FindPools_m34D63087451EE0CB0D502277EE7B9B918BC7370D ();
// 0x0000004F ObjectPoolManager ObjectPoolManager::GetInstance()
extern void ObjectPoolManager_GetInstance_mD3154268984E1FE2CB9803C8A348D11C14241B06 ();
// 0x00000050 System.Void NewBehaviourScript::Start()
extern void NewBehaviourScript_Start_m1209A09215B3F772F03CC987F2B93D495AB98B3C ();
// 0x00000051 System.Void NewBehaviourScript::Update()
extern void NewBehaviourScript_Update_m6139180BDD5B6C84B84DB3C5971F084CA4AB3119 ();
// 0x00000052 System.Void NewBehaviourScript::.ctor()
extern void NewBehaviourScript__ctor_m29866120D313B0941E6B2E57E237DB12EE2294E2 ();
// 0x00000053 System.Void QuestManager::Start()
extern void QuestManager_Start_m42AD522F0AAD579A9AF5BD81B49DBDACD6385A3B ();
// 0x00000054 System.Void QuestManager::Update()
extern void QuestManager_Update_m5B0634B93B1EE6F20FE5A8CE6DB989D79CF635BF ();
// 0x00000055 System.Void QuestManager::OnTriggerEnter(UnityEngine.Collider)
extern void QuestManager_OnTriggerEnter_m5EF5CAE7328028AF302494E780F67A1DCFF208FB ();
// 0x00000056 System.Void QuestManager::.ctor()
extern void QuestManager__ctor_m64EFCC53CCE02A328C24797DCA45288096D29006 ();
// 0x00000057 System.Void QuestObserver::OnNotify(System.String,ObserverPattern.Event)
extern void QuestObserver_OnNotify_m685EC91B626B99A4104E2EA433DD3A5861263260 ();
// 0x00000058 System.Void QuestObserver::.ctor()
extern void QuestObserver__ctor_mE746FAAEA64EF3B03547C8C92A7340F7BFAF1376 ();
// 0x00000059 System.Void QuestUIManager::Start()
extern void QuestUIManager_Start_m2E27453BC9ED311A73C86DB8C2D2DB29FB1F8746 ();
// 0x0000005A System.Void QuestUIManager::UpdateUI()
extern void QuestUIManager_UpdateUI_m1388875BE3750F7F905774835329E3F33B87DE3D ();
// 0x0000005B System.Void QuestUIManager::AddQuest(System.String)
extern void QuestUIManager_AddQuest_m151B692A62E307F1B69FD251D985C1C59496BF05 ();
// 0x0000005C System.Void QuestUIManager::FinishQuest(System.String)
extern void QuestUIManager_FinishQuest_mF436F2CEAF3BD4957BE0CCBD94C520392003C938 ();
// 0x0000005D System.Void QuestUIManager::.ctor()
extern void QuestUIManager__ctor_mB6522A3822C63900234730082FD1B4B3AC951C53 ();
// 0x0000005E System.Void UIObserver::OnNotify(System.String,ObserverPattern.Event)
extern void UIObserver_OnNotify_m240B6407311DE176A4BB46548E88E56873039D06 ();
// 0x0000005F System.Void UIObserver::.ctor()
extern void UIObserver__ctor_m86F3F3E24034D5F20A96EE2AF689617E0B558E69 ();
// 0x00000060 System.Void Bindings::.ctor()
extern void Bindings__ctor_m4109A20A4F1F3A2DC9B0377C11A921C6C49847F6 ();
// 0x00000061 System.Void Bindings::.cctor()
extern void Bindings__cctor_mFCA6CFFBCD056678BBEFA0A1B6F36A3232E90648 ();
// 0x00000062 System.Void PlayerController::checkGrounded()
extern void PlayerController_checkGrounded_m7366FF305CCF755A2B9F49DB18CF1BAC85B6CFCB ();
// 0x00000063 System.Void PlayerController::stickToGround()
extern void PlayerController_stickToGround_mA95F948E7F1BEAB50648E9894AAAD2298EF8BCE1 ();
// 0x00000064 System.Void PlayerController::Start()
extern void PlayerController_Start_m37E4A9612CC78B422FBFC15D257767AE6F969D78 ();
// 0x00000065 System.Void PlayerController::OnDrawGizmos()
extern void PlayerController_OnDrawGizmos_m0F567D5A1EE873D4C5DEE2FD05B16B06D1B74EAF ();
// 0x00000066 System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m16AA033B467FF810A9C1151B616BC1D4B74F7A0E ();
// 0x00000067 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF66473FEFF2DFEB0325A4D9C917C27C6CE4B22F7 ();
// 0x00000068 System.Void PlayerController::.cctor()
extern void PlayerController__cctor_m1C297DE36955EB8EE4D78885783DA0B15390DC56 ();
// 0x00000069 System.Void Util.MetricsLogger::InitLogger()
extern void MetricsLogger_InitLogger_mF39D8CA2BA801B885352AFBED02E9FAF306259B4 ();
// 0x0000006A System.Void Util.MetricsLogger::SetActiveUser(System.String)
extern void MetricsLogger_SetActiveUser_mFB1194939E5D4C0BBE55A32149BC7F20F9822E2B ();
// 0x0000006B System.Void Util.MetricsLogger::SetActiveUserStat(Util.StatsCS)
extern void MetricsLogger_SetActiveUserStat_m9071B33E880FDEB7D5F0A6822EF0D77925FF4C7E ();
// 0x0000006C Util.StatsCS Util.MetricsLogger::GetActiveUserStat()
extern void MetricsLogger_GetActiveUserStat_mB530FF4F3C1A1CC17D11BD2094A036699272A4B4 ();
// 0x0000006D System.Void Util.MetricsLogger::SetTimePlayed(System.Single)
extern void MetricsLogger_SetTimePlayed_m1D3C639CF0DD5646396E2B1E6E019AEDEE45A5E3 ();
// 0x0000006E System.Single Util.MetricsLogger::GetTimePlayed()
extern void MetricsLogger_GetTimePlayed_mCBEC9614D5FD24911BC81D09F367318457D5D9CC ();
// 0x0000006F System.Void Util.MetricsLogger::SetDummiesKilled(System.Int32)
extern void MetricsLogger_SetDummiesKilled_m8FD47743380582269D82E19F7288AA7839539FD0 ();
// 0x00000070 System.Int32 Util.MetricsLogger::GetDummiesKilled()
extern void MetricsLogger_GetDummiesKilled_mEF96DF9D4D0FBFAE7D471997ECD0F47635EFB3DC ();
// 0x00000071 System.Void Util.MetricsLogger::SetShotsHit(System.Single)
extern void MetricsLogger_SetShotsHit_m8A9F5E152741703B983BEF0A40DDD78F531919C3 ();
// 0x00000072 System.Void Util.MetricsLogger::SetShotsFired(System.Single)
extern void MetricsLogger_SetShotsFired_mA53FA3FEBB19280E74EB0F33F5142A5C25AA1120 ();
// 0x00000073 System.Single Util.MetricsLogger::GetShotsHit()
extern void MetricsLogger_GetShotsHit_mD9601B5CF1F8C8B1FC77C14A78A847B8505CE24E ();
// 0x00000074 System.Single Util.MetricsLogger::GetShotsFired()
extern void MetricsLogger_GetShotsFired_m6377EF7657D02DDDC826CE63B636EC1F0DFA347B ();
// 0x00000075 System.Single Util.MetricsLogger::GetAccuracy()
extern void MetricsLogger_GetAccuracy_mD3ED2B28653D109F92FF777A08FF7772BEADF3F0 ();
// 0x00000076 System.Void Util.MetricsLogger::CloseLogger()
extern void MetricsLogger_CloseLogger_mD4946A961D203AAB21E18BAB8A02F039A75339BC ();
// 0x00000077 System.Void Util.MetricsLogger::Start()
extern void MetricsLogger_Start_mF40AF093CC7DD49C1771072381DAD852D2E10432 ();
// 0x00000078 System.Void Util.MetricsLogger::Update()
extern void MetricsLogger_Update_m5484B712F0E117C9A229148882C49B735957D10F ();
// 0x00000079 System.Void Util.MetricsLogger::OnDestroy()
extern void MetricsLogger_OnDestroy_m03F31E9819C44D23B4A7AC8AE08EFBC3A467BF37 ();
// 0x0000007A System.Void Util.MetricsLogger::.ctor()
extern void MetricsLogger__ctor_m6E3B2AA9083F9718FFFB7522D0ECE8B708884C79 ();
// 0x0000007B System.Void Util.MetricsLogger::.cctor()
extern void MetricsLogger__cctor_m6819EF4A387B5689E921EC6A8F27F8A11E90E158 ();
// 0x0000007C System.Void Util.StatsCS::UpdateAccuracy()
extern void StatsCS_UpdateAccuracy_m4283B158BB12055BA9A92070EEFC960ECAD54481_AdjustorThunk ();
// 0x0000007D System.Boolean Quests.Quest::GetIsStarted()
extern void Quest_GetIsStarted_mC1595460B74EACFEF398B5F07339815DA56120B8 ();
// 0x0000007E System.Boolean Quests.Quest::GetIsFinished()
extern void Quest_GetIsFinished_m14ECD0B1E89516640BB1A1F83C6CD59DBCAEEC7B ();
// 0x0000007F System.Void Quests.Quest::SetIsStarted(System.Boolean)
extern void Quest_SetIsStarted_mDDA2088B376215ACCEAD7E57CAF146177A0EC197 ();
// 0x00000080 System.Void Quests.Quest::SetIsFinished(System.Boolean)
extern void Quest_SetIsFinished_m093CF73E61E9C506023DF804E338056BA67B97FD ();
// 0x00000081 System.Void Quests.Quest::Notify(Quests.Quest,ObserverPattern.Event)
extern void Quest_Notify_m16698523E7622CFB1AE9012E6C671DBBAA3846A7 ();
// 0x00000082 System.Void Quests.Quest::.ctor()
extern void Quest__ctor_m8F8A51FA15E80ECE9291D396235CCC44CA53496F ();
// 0x00000083 System.Void Quests.MoveQuest::Update()
extern void MoveQuest_Update_mAC2B50A91D771A3E0DEDC2952483CB96B2770E83 ();
// 0x00000084 System.Void Quests.MoveQuest::.ctor()
extern void MoveQuest__ctor_mE942DBA86577F60EFA6F7AD26F3A3547715E7263 ();
// 0x00000085 System.Void Quests.RotateQuest::Update()
extern void RotateQuest_Update_mE205A026A219298453EF00EED1265858792186CA ();
// 0x00000086 System.Void Quests.RotateQuest::.ctor()
extern void RotateQuest__ctor_m547EE5F15167185B507822A0DDBDB42885FE0084 ();
// 0x00000087 System.Void Quests.DummyAttackQuest::Update()
extern void DummyAttackQuest_Update_mE1274BAB58B120D097A0262AC3BFA1959BA67B87 ();
// 0x00000088 System.Void Quests.DummyAttackQuest::.ctor()
extern void DummyAttackQuest__ctor_mE90547BF69BCC07C240DB70165A4FA71B1932375 ();
// 0x00000089 System.Void Quests.DummyAttackQuest::.cctor()
extern void DummyAttackQuest__cctor_mBB32881E975C56E8E9465B34D7142C9D6018424E ();
// 0x0000008A System.Void Quests.SlowDownQuest::Update()
extern void SlowDownQuest_Update_m3E5662FAC35BBF8A05729E10068A5670E3D54DC7 ();
// 0x0000008B System.Void Quests.SlowDownQuest::.ctor()
extern void SlowDownQuest__ctor_m903BE063FFE5D4DF0C23B44D868ECF507CE2A2FF ();
// 0x0000008C System.Void Quests.FightDummiesQuestPart2ElectricBoogaloo::Update()
extern void FightDummiesQuestPart2ElectricBoogaloo_Update_mC83CEAD9B503A6F47ABBAFBF0617E5DAF2229466 ();
// 0x0000008D System.Void Quests.FightDummiesQuestPart2ElectricBoogaloo::.ctor()
extern void FightDummiesQuestPart2ElectricBoogaloo__ctor_m2B4CED38297EA63017A3351AB761D053238C57E6 ();
// 0x0000008E System.Void Quests.FightDummiesQuestPart2ElectricBoogaloo::.cctor()
extern void FightDummiesQuestPart2ElectricBoogaloo__cctor_m2E1160BE76761000147E68423A8340F8D2519166 ();
// 0x0000008F System.Void Quests.CrouchJumpQuest::Update()
extern void CrouchJumpQuest_Update_mBB5597B359BBA22FCD13384A358AB23CD5388C6F ();
// 0x00000090 System.Void Quests.CrouchJumpQuest::.ctor()
extern void CrouchJumpQuest__ctor_m3393093B627FDE349BE5C14DB22E9B066B8325E9 ();
// 0x00000091 System.Void Quests.FireballQuest::Update()
extern void FireballQuest_Update_mD341514329303E78F0D728B2117AA5CE9775A51E ();
// 0x00000092 System.Void Quests.FireballQuest::.ctor()
extern void FireballQuest__ctor_m77F2A4BD7FAC7FEF383F0C25B7C3110667E5298E ();
// 0x00000093 System.Void Quests.ChainLightningQuest::Update()
extern void ChainLightningQuest_Update_m78803A6489A39E90B5618BC841BA2E4710BCD9C5 ();
// 0x00000094 System.Void Quests.ChainLightningQuest::.ctor()
extern void ChainLightningQuest__ctor_m3E649986E907EC300651B9A1F3A75F90BCAF453A ();
// 0x00000095 System.Void Quests.MagicMissileQuest::Update()
extern void MagicMissileQuest_Update_m857A2B87533F849B531CC7E10AF972F2CBB7573D ();
// 0x00000096 System.Void Quests.MagicMissileQuest::.ctor()
extern void MagicMissileQuest__ctor_m96AD0FFE2CD1A0BFB8C41FB8D1CAD0A8FFCBFE11 ();
// 0x00000097 System.Void Quests.UseSpellQuest::Update()
extern void UseSpellQuest_Update_m87920D05926D2E85AE9FE4F8916A43B3C25DC815 ();
// 0x00000098 System.Void Quests.UseSpellQuest::.ctor()
extern void UseSpellQuest__ctor_mFD74F508552D7C6FD2AC33ED2301F1C99B23A45C ();
// 0x00000099 System.Void ObserverPattern.Observer::OnNotify(System.String,ObserverPattern.Event)
extern void Observer_OnNotify_m578448CA68BDEDB2FD036DC64366D528AE36BC8D ();
// 0x0000009A System.Void ObserverPattern.Observer::.ctor()
extern void Observer__ctor_mB4DC17979A576B95E92CB50136DC1CA2072783C5 ();
// 0x0000009B System.Void ObserverPattern.Subject::AddObserver(ObserverPattern.Observer)
extern void Subject_AddObserver_m6D2DC8B2D29BEDFB256516E8B61A267B6C9E29D1 ();
// 0x0000009C System.Void ObserverPattern.Subject::RemoveObserver(ObserverPattern.Observer)
extern void Subject_RemoveObserver_mCE25B1AB3405E21E825CA390987B9BA2EF29D078 ();
// 0x0000009D System.Void ObserverPattern.Subject::Notify(UnityEngine.Object,ObserverPattern.Event)
extern void Subject_Notify_mB73F6BA267CEA3571FCD479449E5C2E33B371785 ();
// 0x0000009E System.Void ObserverPattern.Subject::Update()
// 0x0000009F System.Void ObserverPattern.Subject::.ctor()
extern void Subject__ctor_mE526D62C75A48B987813B452A4B7F9A0DBCACAB9 ();
// 0x000000A0 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m94E4EDA2F909FACDA1591F7242109AFB650640AA ();
// 0x000000A1 System.Void ChainLightning_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m9A66077BE961C9A744F22C5D6B967007FC9A8B80 ();
// 0x000000A2 System.Single ChainLightning_<>c__DisplayClass9_0::<OnTriggerEnter>b__0(Shootable)
extern void U3CU3Ec__DisplayClass9_0_U3COnTriggerEnterU3Eb__0_m6AB80CE12CCA151BA0374FB96CC25EDF8AE66601 ();
// 0x000000A3 System.Void ObjectPoolManager_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mCF46A1639362AFFF5E9E73E62FB18606A8C93D99 ();
// 0x000000A4 System.Boolean ObjectPoolManager_<>c__DisplayClass7_0::<FindPools>b__0(System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Queue`1<UnityEngine.GameObject>>)
extern void U3CU3Ec__DisplayClass7_0_U3CFindPoolsU3Eb__0_mB84DA67368E1AFBC6CED86CA2ABE307C05208181 ();
static Il2CppMethodPointer s_methodPointers[164] = 
{
	RenderNormalTex_Start_m0F0304CF50704B43665FF8A08E5F624FB7FB6C89,
	RenderNormalTex__ctor_m29702AD449C586425B292EBCE0AE2F17320F8FB1,
	RobertsCrossOutline__ctor_m349072DCDF1624B77573A6250CED4D0352394733,
	RobertsCrossOutlineRenderer_Render_mE969B77DAE3A3A1F72657EC29853C031FC5700C5,
	RobertsCrossOutlineRenderer__ctor_m5061522BFE0C08460C1DB3AF8F163DB5995BB540,
	GunHandler_Start_m2822DA132E5DBE81DC4DA3BDC11D82E81E2544F9,
	GunHandler_Update_mBEEEDE8ECF48CD4D4E08863BF86A8723F82EF4B0,
	GunHandler_Fire_m5ED4DD0E042957D1F0B6BCEE5398900F7677D7C3,
	GunHandler_updateAmmoHud_mAEB33FE32C04364E783BA08C76E7F493968ADC4F,
	GunHandler_dryFire_mB50AE26BF459A7DB9A22138C8E51D502E847FC14,
	GunHandler_reload_m7560C136992B9E49E24AAEAF3A8A60A76C2F1DEF,
	GunHandler_updateWeapon_m73EB8B2F18F12C4E346D0430CE6241027FE405F5,
	GunHandler__ctor_m5B55A71AAF92AE08EF7B746C3A38EB8AF19FAA77,
	Guns__ctor_mAD180587AA05F9041BAB28DBB7D08A3425F27EF8,
	GunSwap_Update_mA362B35D4CA4DE71790E5E6C07AB73159CBB252D,
	GunSwap__ctor_m57BFBE216176EA2D13DDBAC0012247974ADB5B87,
	DummyDie_Start_mB454721F4386BF260E4E20D51081E6DD24958AF6,
	DummyDie_Update_m78D8EE66B78F630F055E7253A96210FBF8D2190E,
	DummyDie__ctor_m33050536DDB42656AF4D7E2F3CC2A4FBF00E4185,
	EnemySpanwer_Start_m2A09ADDBC5640C1BCFE2E31F101730AA51DB4F73,
	EnemySpanwer_Update_m83FAFBB19B6DEC949EF8CA6E6AD0243993DE6DDD,
	EnemySpanwer__ctor_m06F5873FF792FF12D3A44567DB28B92A2B146440,
	Foot_Update_m59F7B517606C811F33BE058FDD686F2C4796C6B5,
	Foot_Step_mD0D293A76FBD362B823F73017B5DBF3D5B3F5E18,
	Foot_Restart_m94971F8565D4CA88C16E1216483644256610467D,
	Foot__ctor_mAC0D26757825E48575EAD3399BDB12CA7B6F2C7D,
	HoverBall_Start_m1C07FA164F357E857DF4934F9AEFBB951F6C1D5A,
	HoverBall_Update_m6BCD403C77D2AA1BDA9ACB9C59FBC96E7722EAA5,
	HoverBall_FixedUpdate_m0C2502A1A70DC4BFAE8F8249BF18C46C6AFA1459,
	HoverBall_OnDrawGizmos_m6DFA2F5BF4BA219CE21A00BB42EBA64ECE15CA5A,
	HoverBall__ctor_m9B16105ACDEFFA8C7FFBE39B9E4621A58CAC0B48,
	NavmeshAI_Start_m086124CDD4F460FEADF6B4EA8F842590A28EF908,
	NavmeshAI_Update_m370E2324FF79360B691942027E79BF38F828AF13,
	NavmeshAI__ctor_mCCA744D57B9DB412CB204CDC25F293323712F2B5,
	SpawnSelfOnDeactivate_OnApplicationQuit_m6171CFAA96C5D19402555E79D2387933ED1DF511,
	SpawnSelfOnDeactivate_OnDisable_m376406D17232981AC218D130C54C8D50F12A192C,
	SpawnSelfOnDeactivate__ctor_m535A79F5898A36D2CD23B6884B94413232A29AE1,
	SpawnSelfOnDie_OnApplicationQuit_mC5A58B4A96BB5E737A365AA48B3344ED935F7B33,
	SpawnSelfOnDie_OnDestroy_mF9E8944D8878FB87E41C25969B8D511C86EB5A2D,
	SpawnSelfOnDie__ctor_m4039CFB0811DE19CE2656860C266F1D9912D9553,
	ChainLightning_Awake_m6E0DF75604A51D7BC17F05B676275DA2EE626BB3,
	ChainLightning_Update_mDE0040864F4CBD840A778AA1E15D4CD84048A0E2,
	ChainLightning_OnTriggerEnter_m4845910BB048BE756019CF01B0E1AED12CF90D06,
	ChainLightning__ctor_m8D6AE9E7E97EB3D430E9FE3C7C3FB542E1F09B4C,
	ExplosionLogic_Awake_mC475951C99AF5674DB8661D579017E34469F3689,
	ExplosionLogic_Update_m3E1AFAB0B30838F02A8B8E9EDA82BDB9F6C0B2E6,
	ExplosionLogic_OnTriggerEnter_m4654E5915B04EF9B07DF4809F62649DFE3A1D8BB,
	ExplosionLogic_OnDrawGizmos_m6DE525778043695E684BC7CAEFD8C8270ECB110D,
	ExplosionLogic__ctor_mE44FB9A9A4526DE9286F38EFB60D501CBDF6E9DE,
	Fireball_Awake_mCF516BFAF355DBD2AFE03799764774B03862B711,
	Fireball_OnCollisionEnter_m560D6607D33B367E6ABC11C38B0CECC9E37F779E,
	Fireball__ctor_mC7C51E9F63A9C13D675C35E310B04E404FC68C58,
	KillAfterTime_Awake_mEE7E0EE26AE4484E11BFC17614B5FB0683D33BDD,
	KillAfterTime_Update_mCB5157EBFCAD785A6E9667F6DE6F1952E7F74897,
	KillAfterTime__ctor_m0C677F602EDD44351E7AD9126A91C76CA8E4E980,
	MagicMissile_Start_mD8FEEB465DA4EB30F5B89EDC835DE3A665BB0471,
	MagicMissile_Update_m0B35400AB6AA3FF63820EFC63B0B6BCCC9ADC77F,
	MagicMissile_FixedUpdate_mCB4E5057CC417059767634ADCF413EFDAC05362D,
	MagicMissile_OnCollisionEnter_m74CE185B4808211F4CD1CC8E3A6A2D521551B4DE,
	MagicMissile__ctor_m06EB55A88861F31E3C3ACAE21A649160D39BE704,
	Spell__ctor_m43A4A689F2E85395D858EFE1181B98DBC19EC41B,
	SpellController_Start_mD92203C47CC08FE909D69B02FAC9B30CE324E779,
	SpellController_Update_m98BFED179FBE125F09E282DCB0466C2D89F66861,
	SpellController__ctor_mB3582C212C2BD439304EC77ECE88C2A1D349FA7A,
	SpellData__ctor_m0FB708800B8514D090D242470813647F9928A64C,
	Shootable_Start_m77E171A94C2CE67AB1E72E046AFF59F70656E630,
	Shootable_TakeDamage_mBD5C79B687356B2CA5510C019C7CE3271BE724B6,
	Shootable_ResetHealth_mC25DBBF80946AC5BAE42C51B87666669663A6D87,
	Shootable_Die_m65292A58A36966E02E9B4F1AFCD27EDBB68F0AF0,
	Shootable__ctor_mBEF8970F3132EBD8498449742E21F6A0DBAF544E,
	ShootablePool_Die_m700296F2E5E5BEFB8E79FFF10E698837DD2B1C8A,
	ShootablePool__ctor_m183B3460651A5BFEA0D0338290DEE77CDFA03520,
	ObjectPoolManager__ctor_mD953D052C283DB2A5B1BD959FE9774DB3DE02520,
	ObjectPoolManager_InitPool_m7F2DDE9647DEBD656E1BC80AE11888C48D093921,
	ObjectPoolManager_GetPool_mB954C06D49579A6AA5CC9886763802C071D3C0E5,
	ObjectPoolManager_GetObject_m5DCFCB7894165B82392FF9A9BC13416F2CF06FFE,
	ObjectPoolManager_PoolObject_mE6A2332012CDBFD60729BF6058FFC1F531AA1FD3,
	ObjectPoolManager_FindPools_m34D63087451EE0CB0D502277EE7B9B918BC7370D,
	ObjectPoolManager_GetInstance_mD3154268984E1FE2CB9803C8A348D11C14241B06,
	NewBehaviourScript_Start_m1209A09215B3F772F03CC987F2B93D495AB98B3C,
	NewBehaviourScript_Update_m6139180BDD5B6C84B84DB3C5971F084CA4AB3119,
	NewBehaviourScript__ctor_m29866120D313B0941E6B2E57E237DB12EE2294E2,
	QuestManager_Start_m42AD522F0AAD579A9AF5BD81B49DBDACD6385A3B,
	QuestManager_Update_m5B0634B93B1EE6F20FE5A8CE6DB989D79CF635BF,
	QuestManager_OnTriggerEnter_m5EF5CAE7328028AF302494E780F67A1DCFF208FB,
	QuestManager__ctor_m64EFCC53CCE02A328C24797DCA45288096D29006,
	QuestObserver_OnNotify_m685EC91B626B99A4104E2EA433DD3A5861263260,
	QuestObserver__ctor_mE746FAAEA64EF3B03547C8C92A7340F7BFAF1376,
	QuestUIManager_Start_m2E27453BC9ED311A73C86DB8C2D2DB29FB1F8746,
	QuestUIManager_UpdateUI_m1388875BE3750F7F905774835329E3F33B87DE3D,
	QuestUIManager_AddQuest_m151B692A62E307F1B69FD251D985C1C59496BF05,
	QuestUIManager_FinishQuest_mF436F2CEAF3BD4957BE0CCBD94C520392003C938,
	QuestUIManager__ctor_mB6522A3822C63900234730082FD1B4B3AC951C53,
	UIObserver_OnNotify_m240B6407311DE176A4BB46548E88E56873039D06,
	UIObserver__ctor_m86F3F3E24034D5F20A96EE2AF689617E0B558E69,
	Bindings__ctor_m4109A20A4F1F3A2DC9B0377C11A921C6C49847F6,
	Bindings__cctor_mFCA6CFFBCD056678BBEFA0A1B6F36A3232E90648,
	PlayerController_checkGrounded_m7366FF305CCF755A2B9F49DB18CF1BAC85B6CFCB,
	PlayerController_stickToGround_mA95F948E7F1BEAB50648E9894AAAD2298EF8BCE1,
	PlayerController_Start_m37E4A9612CC78B422FBFC15D257767AE6F969D78,
	PlayerController_OnDrawGizmos_m0F567D5A1EE873D4C5DEE2FD05B16B06D1B74EAF,
	PlayerController_FixedUpdate_m16AA033B467FF810A9C1151B616BC1D4B74F7A0E,
	PlayerController__ctor_mF66473FEFF2DFEB0325A4D9C917C27C6CE4B22F7,
	PlayerController__cctor_m1C297DE36955EB8EE4D78885783DA0B15390DC56,
	MetricsLogger_InitLogger_mF39D8CA2BA801B885352AFBED02E9FAF306259B4,
	MetricsLogger_SetActiveUser_mFB1194939E5D4C0BBE55A32149BC7F20F9822E2B,
	MetricsLogger_SetActiveUserStat_m9071B33E880FDEB7D5F0A6822EF0D77925FF4C7E,
	MetricsLogger_GetActiveUserStat_mB530FF4F3C1A1CC17D11BD2094A036699272A4B4,
	MetricsLogger_SetTimePlayed_m1D3C639CF0DD5646396E2B1E6E019AEDEE45A5E3,
	MetricsLogger_GetTimePlayed_mCBEC9614D5FD24911BC81D09F367318457D5D9CC,
	MetricsLogger_SetDummiesKilled_m8FD47743380582269D82E19F7288AA7839539FD0,
	MetricsLogger_GetDummiesKilled_mEF96DF9D4D0FBFAE7D471997ECD0F47635EFB3DC,
	MetricsLogger_SetShotsHit_m8A9F5E152741703B983BEF0A40DDD78F531919C3,
	MetricsLogger_SetShotsFired_mA53FA3FEBB19280E74EB0F33F5142A5C25AA1120,
	MetricsLogger_GetShotsHit_mD9601B5CF1F8C8B1FC77C14A78A847B8505CE24E,
	MetricsLogger_GetShotsFired_m6377EF7657D02DDDC826CE63B636EC1F0DFA347B,
	MetricsLogger_GetAccuracy_mD3ED2B28653D109F92FF777A08FF7772BEADF3F0,
	MetricsLogger_CloseLogger_mD4946A961D203AAB21E18BAB8A02F039A75339BC,
	MetricsLogger_Start_mF40AF093CC7DD49C1771072381DAD852D2E10432,
	MetricsLogger_Update_m5484B712F0E117C9A229148882C49B735957D10F,
	MetricsLogger_OnDestroy_m03F31E9819C44D23B4A7AC8AE08EFBC3A467BF37,
	MetricsLogger__ctor_m6E3B2AA9083F9718FFFB7522D0ECE8B708884C79,
	MetricsLogger__cctor_m6819EF4A387B5689E921EC6A8F27F8A11E90E158,
	StatsCS_UpdateAccuracy_m4283B158BB12055BA9A92070EEFC960ECAD54481_AdjustorThunk,
	Quest_GetIsStarted_mC1595460B74EACFEF398B5F07339815DA56120B8,
	Quest_GetIsFinished_m14ECD0B1E89516640BB1A1F83C6CD59DBCAEEC7B,
	Quest_SetIsStarted_mDDA2088B376215ACCEAD7E57CAF146177A0EC197,
	Quest_SetIsFinished_m093CF73E61E9C506023DF804E338056BA67B97FD,
	Quest_Notify_m16698523E7622CFB1AE9012E6C671DBBAA3846A7,
	Quest__ctor_m8F8A51FA15E80ECE9291D396235CCC44CA53496F,
	MoveQuest_Update_mAC2B50A91D771A3E0DEDC2952483CB96B2770E83,
	MoveQuest__ctor_mE942DBA86577F60EFA6F7AD26F3A3547715E7263,
	RotateQuest_Update_mE205A026A219298453EF00EED1265858792186CA,
	RotateQuest__ctor_m547EE5F15167185B507822A0DDBDB42885FE0084,
	DummyAttackQuest_Update_mE1274BAB58B120D097A0262AC3BFA1959BA67B87,
	DummyAttackQuest__ctor_mE90547BF69BCC07C240DB70165A4FA71B1932375,
	DummyAttackQuest__cctor_mBB32881E975C56E8E9465B34D7142C9D6018424E,
	SlowDownQuest_Update_m3E5662FAC35BBF8A05729E10068A5670E3D54DC7,
	SlowDownQuest__ctor_m903BE063FFE5D4DF0C23B44D868ECF507CE2A2FF,
	FightDummiesQuestPart2ElectricBoogaloo_Update_mC83CEAD9B503A6F47ABBAFBF0617E5DAF2229466,
	FightDummiesQuestPart2ElectricBoogaloo__ctor_m2B4CED38297EA63017A3351AB761D053238C57E6,
	FightDummiesQuestPart2ElectricBoogaloo__cctor_m2E1160BE76761000147E68423A8340F8D2519166,
	CrouchJumpQuest_Update_mBB5597B359BBA22FCD13384A358AB23CD5388C6F,
	CrouchJumpQuest__ctor_m3393093B627FDE349BE5C14DB22E9B066B8325E9,
	FireballQuest_Update_mD341514329303E78F0D728B2117AA5CE9775A51E,
	FireballQuest__ctor_m77F2A4BD7FAC7FEF383F0C25B7C3110667E5298E,
	ChainLightningQuest_Update_m78803A6489A39E90B5618BC841BA2E4710BCD9C5,
	ChainLightningQuest__ctor_m3E649986E907EC300651B9A1F3A75F90BCAF453A,
	MagicMissileQuest_Update_m857A2B87533F849B531CC7E10AF972F2CBB7573D,
	MagicMissileQuest__ctor_m96AD0FFE2CD1A0BFB8C41FB8D1CAD0A8FFCBFE11,
	UseSpellQuest_Update_m87920D05926D2E85AE9FE4F8916A43B3C25DC815,
	UseSpellQuest__ctor_mFD74F508552D7C6FD2AC33ED2301F1C99B23A45C,
	Observer_OnNotify_m578448CA68BDEDB2FD036DC64366D528AE36BC8D,
	Observer__ctor_mB4DC17979A576B95E92CB50136DC1CA2072783C5,
	Subject_AddObserver_m6D2DC8B2D29BEDFB256516E8B61A267B6C9E29D1,
	Subject_RemoveObserver_mCE25B1AB3405E21E825CA390987B9BA2EF29D078,
	Subject_Notify_mB73F6BA267CEA3571FCD479449E5C2E33B371785,
	NULL,
	Subject__ctor_mE526D62C75A48B987813B452A4B7F9A0DBCACAB9,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m94E4EDA2F909FACDA1591F7242109AFB650640AA,
	U3CU3Ec__DisplayClass9_0__ctor_m9A66077BE961C9A744F22C5D6B967007FC9A8B80,
	U3CU3Ec__DisplayClass9_0_U3COnTriggerEnterU3Eb__0_m6AB80CE12CCA151BA0374FB96CC25EDF8AE66601,
	U3CU3Ec__DisplayClass7_0__ctor_mCF46A1639362AFFF5E9E73E62FB18606A8C93D99,
	U3CU3Ec__DisplayClass7_0_U3CFindPoolsU3Eb__0_mB84DA67368E1AFBC6CED86CA2ABE307C05208181,
};
static const int32_t s_InvokerIndices[164] = 
{
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2005,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	290,
	23,
	23,
	23,
	23,
	23,
	23,
	600,
	28,
	28,
	27,
	28,
	4,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	136,
	23,
	23,
	23,
	26,
	26,
	23,
	136,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	3,
	122,
	2007,
	2008,
	2009,
	1186,
	133,
	131,
	2009,
	2009,
	1186,
	1186,
	1186,
	3,
	23,
	23,
	23,
	23,
	3,
	23,
	114,
	114,
	31,
	31,
	136,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	136,
	23,
	26,
	26,
	136,
	23,
	23,
	95,
	23,
	192,
	23,
	2006,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	164,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
