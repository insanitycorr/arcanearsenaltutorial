﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean UnityEngine.XR.XRSettings::get_enabled()
extern void XRSettings_get_enabled_m74A0B484E1B6D7187A34EEFFC7CDFD60E3575AA0 ();
// 0x00000002 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureWidth()
extern void XRSettings_get_eyeTextureWidth_m214FD01E2CA4D825BDCB51AD35390BB81DFE36CD ();
// 0x00000003 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureHeight()
extern void XRSettings_get_eyeTextureHeight_m819C153D5D44BAB89F0AF99474AA987471B416B9 ();
// 0x00000004 UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRSettings::get_eyeTextureDesc()
extern void XRSettings_get_eyeTextureDesc_mE8B378ED9A6692FF7E8BE261C46A426F630B69DD ();
// 0x00000005 System.Single UnityEngine.XR.XRSettings::get_renderViewportScale()
extern void XRSettings_get_renderViewportScale_mAD4CE67ED8318B9D26CA9B092EBC592C1E38AE59 ();
// 0x00000006 System.Single UnityEngine.XR.XRSettings::get_renderViewportScaleInternal()
extern void XRSettings_get_renderViewportScaleInternal_m68CF4633C56407C080DD0930AEAC9286AAA304F1 ();
// 0x00000007 System.Void UnityEngine.XR.XRSettings::get_eyeTextureDesc_Injected(UnityEngine.RenderTextureDescriptor&)
extern void XRSettings_get_eyeTextureDesc_Injected_m2E5464BF666C27FB20CD0D0CCA703F40ED382597 ();
// 0x00000008 System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String)
extern void XRDevice_InvokeDeviceLoaded_mD5D5577A4E03D0474FAFBB2596B698B6A8B5FD11 ();
// 0x00000009 System.Void UnityEngine.XR.XRDevice::.cctor()
extern void XRDevice__cctor_m4FE111291FBDF43A481045CBABECF9AEC70B5EC9 ();
static Il2CppMethodPointer s_methodPointers[9] = 
{
	XRSettings_get_enabled_m74A0B484E1B6D7187A34EEFFC7CDFD60E3575AA0,
	XRSettings_get_eyeTextureWidth_m214FD01E2CA4D825BDCB51AD35390BB81DFE36CD,
	XRSettings_get_eyeTextureHeight_m819C153D5D44BAB89F0AF99474AA987471B416B9,
	XRSettings_get_eyeTextureDesc_mE8B378ED9A6692FF7E8BE261C46A426F630B69DD,
	XRSettings_get_renderViewportScale_mAD4CE67ED8318B9D26CA9B092EBC592C1E38AE59,
	XRSettings_get_renderViewportScaleInternal_m68CF4633C56407C080DD0930AEAC9286AAA304F1,
	XRSettings_get_eyeTextureDesc_Injected_m2E5464BF666C27FB20CD0D0CCA703F40ED382597,
	XRDevice_InvokeDeviceLoaded_mD5D5577A4E03D0474FAFBB2596B698B6A8B5FD11,
	XRDevice__cctor_m4FE111291FBDF43A481045CBABECF9AEC70B5EC9,
};
static const int32_t s_InvokerIndices[9] = 
{
	49,
	131,
	131,
	1572,
	1186,
	1186,
	17,
	122,
	3,
};
extern const Il2CppCodeGenModule g_UnityEngine_VRModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VRModuleCodeGenModule = 
{
	"UnityEngine.VRModule.dll",
	9,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
