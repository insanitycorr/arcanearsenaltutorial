#pragma once

#include "User.h"

//Logs metrics
class PLUGIN_API MetricsLogger
{
public:
	//MetricsLogger();
	//~MetricsLogger();
	void InitLogger();

	//Active user stuff
	void SetActiveUser(const char* userName);
	const char* GetActiveUser();
	void SetActiveUserStat(StatsCS stat);
	StatsCS GetActiveUserStat();

	void SetTimePlayed(float _timePlayed);
	float GetTimePlayed();

	void SetDummiesKilled(int _dummiesKilled);
	int GetDummiesKilled();

	void SetShotsHit(float _shotsHit);
	void SetShotsFired(float _shotsFired);
	float GetShotsHit();
	float GetShotsFired();
	float GetAccuracy();

	//Usernames and number of users
	const char* GetUserName(int userNumber);
	int GetNumUsers();

	//Run on close
	void CloseLogger();
private:

};