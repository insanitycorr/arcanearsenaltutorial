#include "Stats.h"

Stats::Stats(StatsCS st)
{
	//Copy from CS version
	this->timePlayed = st.timePlayed;
	this->dummiesKilled = st.dummiesKilled;
	this->shotsHit = st.shotsHit;
	this->shotsFired = st.shotsFired;
	this->accuracy = st.accuracy;
}

void Stats::UpdateAccuracy()
{
	if (shotsFired > 0.f)
	{
		accuracy = (shotsHit / shotsFired) * 100.f;
	}
	else
	{
		accuracy = 100.f;
	}
}

StatsCS Stats::ToCSStats()
{
	StatsCS convertStat;
	convertStat.timePlayed = this->timePlayed;
	convertStat.dummiesKilled = this->dummiesKilled;
	convertStat.shotsHit = this->shotsHit;
	convertStat.shotsFired = this->shotsFired;
	convertStat.accuracy = this->accuracy;

	return convertStat;
}

Stats Stats::operator+(Stats stat2)
{
	Stats temp;
	temp.timePlayed = this->timePlayed + stat2.timePlayed;

	temp.dummiesKilled = this->dummiesKilled + stat2.dummiesKilled;

	temp.shotsHit = this->shotsHit + stat2.shotsHit;
	temp.shotsFired = this->shotsFired + stat2.shotsFired;

	return temp;
}

std::ostream& operator<<(std::ostream& os, const Stats& stats)
{
	os << "Time Played: " << stats.timePlayed << "\n\n";

	os << "Dummies Killed: " << stats.dummiesKilled << "\n\n";

	os << "Accuracy: " << stats.accuracy << "\n";
	os << "Shots Hit: " << stats.shotsHit << "\n";
	os << "Shots Fired: " << stats.shotsFired << "\n";

	return os;
}

void StatsCS::UpdateAccuracy()
{
	if (shotsFired > 0.f)
	{
		accuracy = (shotsHit / shotsFired) * 100.f;
	}
	else
	{
		accuracy = 100.f;
	}
}
