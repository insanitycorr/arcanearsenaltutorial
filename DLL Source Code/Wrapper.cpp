#include "Wrapper.h"

//Put object here
MetricsLogger logger;

//Use wrappers with object to call functions

void InitLogger()
{
	return logger.InitLogger();
}

void SetActiveUser(const char* userName)
{
	return logger.SetActiveUser(userName);;
}

//PLUGIN_API const char* GetActiveUser()
//{
//	return logger.GetActiveUser();
//}

void SetActiveUserStat(StatsCS stat)
{
	return logger.SetActiveUserStat(stat);
}

StatsCS GetActiveUserStat()
{
	return logger.GetActiveUserStat();
}

void SetTimePlayed(float _timePlayed)
{
	return logger.SetTimePlayed(_timePlayed);
}

float GetTimePlayed()
{
	return logger.GetTimePlayed();
}

void SetDummiesKilled(int _dummiesKilled)
{
	return logger.SetDummiesKilled(_dummiesKilled);
}

int GetDummiesKilled()
{
	return logger.GetDummiesKilled();
}

void SetShotsHit(float _shotsHit)
{
	return logger.SetShotsHit(_shotsHit);
}

void SetShotsFired(float _shotsFired)
{
	return logger.SetShotsFired(_shotsFired);
}

float GetShotsHit()
{
	return logger.GetShotsHit();
}

float GetShotsFired()
{
	return logger.GetShotsFired();
}

float GetAccuracy()
{
	return logger.GetAccuracy();
}

void CloseLogger()
{
	return logger.CloseLogger();
}

//
//PLUGIN_API const char* GetUserName(int userNumber)
//{
//	return logger.GetUserName(userNumber);
//}
//
//PLUGIN_API int GetNumUsers()
//{
//	return logger.GetNumUsers();
//}
