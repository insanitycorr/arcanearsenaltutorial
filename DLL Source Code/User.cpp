#include "User.h"
Stats UserManager::m_activeUserSessionStat;
std::string UserManager::m_activeUser = "";

std::vector<std::string> UserManager::m_userNames;
std::map<std::string, nlohmann::json> UserManager::m_jsonList;
std::map<std::string, User> UserManager::m_userList;

UserManager::~UserManager()
{
	//Runs on close
	SaveUsers();
}

void UserManager::InitManager()
{
	nlohmann::json j;
	int numUsers = 0;

	if (File::JSONExist("UserConfig.json"))
	{
		j = File::LoadJSON("UserConfig.json");
		numUsers = j["Num Users"];
	}
	
	for (int i = 0; i < numUsers; i++)
	{
		//Adds all the existing users from UserConfig
		UserManager::AddExistingUser(j["User " + std::to_string(i)]);
	}
}

bool UserManager::UserExists(std::string userName)
{
	if (m_userList.find(userName) == m_userList.end())
	{
		//Not found
		return false;
	}
	//found
	return true;
}

void UserManager::RemoveUser(std::string userName)
{
	//Remove user
	m_userList.erase(userName);
}

std::vector<std::string> UserManager::GetUsers()
{
	return m_userNames;
}

Stats& UserManager::GetActiveUserStat()
{
	m_activeUserSessionStat.UpdateAccuracy();
	return m_activeUserSessionStat;
}

void UserManager::SetActiveUserStat(Stats stats)
{
	stats.UpdateAccuracy();
	m_activeUserSessionStat = stats;
}

void UserManager::SetActiveUser(std::string userName)
{
	if (!UserManager::UserExists(userName) && userName != "")
	{
		//Adds user if it doesn't exist
		UserManager::AddUser(userName);
	}

	//Output current session
	if (m_activeUser != "")
	{
		nlohmann::json& j = m_jsonList[m_activeUser];

		//Get session number
		int numSessions = j["Total Sessions"];
		//store new stats
		j["Session" + std::to_string(numSessions)] = UserManager::GetActiveUserStat();

		//Gets the old lifetime stats
		Stats prevLife = m_jsonList[m_activeUser]["Lifetime Stats"];
		//Adds stats to lifetime 
		Stats newLife = UserManager::GetActiveUserStat() + prevLife;
		//Updates lifetime stats to reflect accuracy
		newLife.UpdateAccuracy();

		//Stores new lifetime
		m_jsonList[m_activeUser]["Lifetime Stats"] = newLife;

		//Increases number of stored sessions
		numSessions++;
		j["Total Sessions"] = numSessions;
	}

	m_activeUser = userName;
	m_activeUserSessionStat = Stats();
}

std::string UserManager::GetActiveUser()
{
	return m_activeUser;
}

void UserManager::SaveUsers()
{
	SetActiveUser("");

	nlohmann::json userConfig;

	int userNumber = 0;
	for (auto user : m_userList)
	{
		userConfig["User " + std::to_string(userNumber)] = user.first;
		userNumber++;

		File::SaveJSON(user.first + ".json", m_jsonList[user.first]);
	}

	userConfig["Num Users"] = userNumber;
	File::SaveJSON("UserConfig.json", userConfig);
}

User UserManager::GetUser(std::string userName)
{
	if (!UserManager::UserExists(userName))
	{
		UserManager::AddUser(userName);
	}
	return m_userList[userName];
}

void UserManager::AddUser(std::string userName)
{
	//Inserts new user
	m_userList.insert(std::pair<std::string, User>(userName, User(userName)));
	m_userNames.push_back(userName);

	//List of jsons representing the users
	m_jsonList.insert(std::pair<std::string, nlohmann::json>(userName, nlohmann::json()));
	//Inits lifetime stats
	m_jsonList[userName]["Lifetime Stats"] = Stats();
	m_jsonList[userName]["Total Sessions"] = 0;
}

void UserManager::AddExistingUser(std::string userName)
{
	//Inserts existing user
	m_userList.insert(std::pair<std::string, User>(userName, User(userName)));
	m_userNames.push_back(userName);

	//List of jsons representing users
	m_jsonList.insert(std::pair<std::string, nlohmann::json>(userName, File::LoadJSON(userName + ".json")));
}
