#pragma once

#ifdef USERMETRICSLOGGER_EXPORTS
#define PLUGIN_API __declspec(dllexport)
#elif USERMETRICSLOGGER_EXPORTS
#define PLUGIN_API __declspec(dllimport)
#else
#define PLUGIN_API
#endif
