#pragma once

#include <vector>
#include "Stats.h"

struct PLUGIN_API User
{
	User()
	{
		//Does nothing
	}

	User(std::string _userName)
	{
		userName = _userName;
	}
	std::string userName = "";
};

class PLUGIN_API UserManager abstract
{
public:
	//Runs on program termination
	~UserManager();

	//Init manager
	static void InitManager();
	
	//Gets a user
	static User GetUser(std::string userName);
	//Removes a user
	static void RemoveUser(std::string userName);

	//Get list of usernames
	static std::vector<std::string> GetUsers();

	//Set active user and get stat
	static Stats& GetActiveUserStat();
	static void SetActiveUserStat(Stats stats);
	static void SetActiveUser(std::string userName);
	static std::string GetActiveUser();

	//To be called upon close
	static void SaveUsers();

private:
	//Does user exist?
	static bool UserExists(std::string userName);

	//Add users and and adds existing users
	static void AddUser(std::string userName);
	static void AddExistingUser(std::string userName);

	//Active user stas
	static Stats m_activeUserSessionStat;
	static std::string m_activeUser;

	//Json and user list
	static std::vector<std::string> m_userNames;
	static std::map<std::string, nlohmann::json> m_jsonList;
	static std::map<std::string, User> m_userList;
};

