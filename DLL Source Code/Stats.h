#pragma once

#include "File.h"
#include "PluginSettings.h"

//Forward declaration
struct PLUGIN_API StatsCS;

struct PLUGIN_API Stats
{
public:
	Stats() { }
	Stats(StatsCS st);

	void UpdateAccuracy();

	//Time Played
	float timePlayed = 0.f;

	//Dummies killed
	int dummiesKilled = 0;
	
	//Accuracy values
	float accuracy = 100.f;
	float shotsHit = 0.f;
	float shotsFired = 0.f;

	StatsCS ToCSStats();

	Stats operator+(Stats stat2);
	friend std::ostream& operator<<(std::ostream& os, const Stats& stats);
};

struct PLUGIN_API StatsCS
{
public:
	void UpdateAccuracy();

	//Time Played
	float timePlayed = 0.f;

	//Dummies killed
	int dummiesKilled = 0;

	//Accuracy values
	float accuracy = 100.f;
	float shotsHit = 0.f;
	float shotsFired = 0.f;
};

//Sends Stats TO json file
inline void to_json(nlohmann::json& j, const Stats& bar)
{
	//Time Played
	j["Time Played"] = bar.timePlayed;

	//How many dummies have you killed
	j["Dummies Killed"] = bar.dummiesKilled;

	//How Accurate were you?
	j["Accuracy"] = bar.accuracy;
	j["Shots Hit"] = bar.shotsHit;
	j["Shots Fired"] = bar.shotsFired;
}

//Sends Stats in FROM json file
inline void from_json(const nlohmann::json& j, Stats& bar)
{
	//Time Played
	bar.timePlayed = j["Time Played"];

	//How many dummies have you killed
	bar.dummiesKilled = j["Dummies Killed"];

	//How Accurate were you?
	bar.accuracy = j["Accuracy"];
	bar.shotsHit = j["Shots Hit"];
	bar.shotsFired = j["Shots Fired"];
}