#include "MetricsLogger.h"

//MetricsLogger::MetricsLogger()
//{
//	//Initializes the manager
//	UserManager::InitManager();
//}
//
//MetricsLogger::~MetricsLogger()
//{
//	CloseLogger();
//}

void MetricsLogger::InitLogger()
{
	UserManager::InitManager();
}

void MetricsLogger::SetActiveUser(const char* userName)
{
	//Sets the active user
	UserManager::SetActiveUser(userName);
}

const char* MetricsLogger::GetActiveUser()
{
	return UserManager::GetActiveUser().c_str();
}

void MetricsLogger::SetActiveUserStat(StatsCS stat)
{
	//Sets the active user stats
	UserManager::SetActiveUserStat(Stats(stat));
}

StatsCS MetricsLogger::GetActiveUserStat()
{
	//Get active user stat
	return UserManager::GetActiveUserStat().ToCSStats();
}

void MetricsLogger::SetTimePlayed(float _timePlayed)
{
	Stats get = UserManager::GetActiveUserStat();

	get.timePlayed = _timePlayed;

	UserManager::SetActiveUserStat(get);
}

float MetricsLogger::GetTimePlayed()
{
	return GetActiveUserStat().timePlayed;
}

void MetricsLogger::SetDummiesKilled(int _dummiesKilled)
{
	Stats get = UserManager::GetActiveUserStat();

	get.dummiesKilled = _dummiesKilled;

	UserManager::SetActiveUserStat(get);
}

int MetricsLogger::GetDummiesKilled()
{
	return GetActiveUserStat().dummiesKilled;
}

void MetricsLogger::SetShotsHit(float _shotsHit)
{
	Stats get = UserManager::GetActiveUserStat();

	get.shotsHit = _shotsHit;

	UserManager::SetActiveUserStat(get);
}

void MetricsLogger::SetShotsFired(float _shotsFired)
{
	Stats get = UserManager::GetActiveUserStat();

	get.shotsFired = _shotsFired;

	UserManager::SetActiveUserStat(get);
}

float MetricsLogger::GetShotsHit()
{
	return UserManager::GetActiveUserStat().shotsHit;
}

float MetricsLogger::GetShotsFired()
{
	return UserManager::GetActiveUserStat().shotsFired;
}

float MetricsLogger::GetAccuracy()
{
	return UserManager::GetActiveUserStat().accuracy;
}

const char* MetricsLogger::GetUserName(int userNumber)
{
	//Grabs a username based on the number in the list
	return UserManager::GetUsers()[userNumber].c_str();
}

int MetricsLogger::GetNumUsers()
{
	//Gets the number of user profiles
	return UserManager::GetUsers().size();
}

void MetricsLogger::CloseLogger()
{
	//Saves the user data
	UserManager::SaveUsers();
}
