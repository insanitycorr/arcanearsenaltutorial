#pragma once

#include "MetricsLogger.h"

#ifdef __cplusplus
extern "C"
{
#endif

	//Initializes the management system
	PLUGIN_API void InitLogger();

	//Sets the active user this session
	PLUGIN_API void SetActiveUser(const char* userName);
	// //Gets the active user this session
	// PLUGIN_API const char* GetActiveUser();
	//Sets the active user stats this session
	PLUGIN_API void SetActiveUserStat(StatsCS stat);
	//Gets the active user this session
	PLUGIN_API StatsCS GetActiveUserStat();

	//Sets the time played this session
	PLUGIN_API void SetTimePlayed(float _timePlayed);
	//Gets the time played this session 
	PLUGIN_API float GetTimePlayed();

	//Sets the number of dummies killed this session
	PLUGIN_API void SetDummiesKilled(int _dummiesKilled);
	//Gets the number of dummies killed this session
	PLUGIN_API int GetDummiesKilled();

	//Sets the number of shots that hit something this session
	PLUGIN_API void SetShotsHit(float _shotsHit);
	//Sets the number of shots that were fired this session
	PLUGIN_API void SetShotsFired(float _shotsFired);
	//Gets the number of shots that hit something this session
	PLUGIN_API float GetShotsHit();
	//Gets the number of shots that were fired this session
	PLUGIN_API float GetShotsFired();
	//Gets the accuracy of the user this session
	PLUGIN_API float GetAccuracy();

	PLUGIN_API void CloseLogger();

	// PLUGIN_API const char* GetUserName(int userNumber);
	// PLUGIN_API int GetNumUsers();
	
#ifdef __cplusplus
}
#endif