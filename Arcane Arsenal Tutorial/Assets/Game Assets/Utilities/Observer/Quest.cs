﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObserverPattern;

namespace Quests
{
    public abstract class Quest : Subject
    {
        protected bool isStarted = false;
        protected bool isFinished = false;

        public bool GetIsStarted()
        {
            return isStarted;
        }
        public bool GetIsFinished()
        {
            return isFinished;
        }

        public void SetIsStarted(bool started)
        {
            if (started != isStarted && !isStarted)
            {
                Notify(this, ObserverPattern.Event.QUEST_STARTED);
            }

            isStarted = started;
        }

        public void SetIsFinished(bool finished)
        {
            if (finished != isFinished && !isFinished)
            {
                Notify(this, ObserverPattern.Event.QUEST_ENDED);
            }

            isFinished = finished;
        }

        public void Notify(Quest theQuest, ObserverPattern.Event theEvent)
        {
            foreach (Observer observer in observers)
            {
                observer.OnNotify(theQuest.GetType().Name, theEvent);
            }
        }
    }

    public class MoveQuest : Quest
    {
        public override void Update()
        {
            //Check if the moving has started
            if (isStarted && !isFinished)
            {
                if (Input.GetAxis("Horizontal") > 0.25f || Input.GetAxis("Vertical") > 0.25f)
                    SetIsFinished(true);
            }
        }
    }

    public class RotateQuest : Quest
    {

        public override void Update()
        {
            //Has the rotate quest been started?
            if (isStarted && !isFinished)
            {
                if (Input.GetAxis("Mouse X") > 0.25f || Input.GetAxis("Mouse Y") > 0.25f)
                    SetIsFinished(true);
            }
        }
    }

    public class DummyAttackQuest : Quest
    {
        public static int numDummy = 1;
        public override void Update()
        {
            //Has the dummy attack quest been started?
            if (isStarted && !isFinished)
            {
                //Check if the dummies are dead
                if (Util.MetricsLogger.sessionStats.dummiesKilled >= numDummy)
                {
                    SetIsFinished(true);
                }
            }
        }
    }

    public class SlowDownQuest : Quest
    {
        public override void Update()
        {
            //Has the slow down quest been started?
            if (isStarted && !isFinished)
            {
                //Check if the you've gotten across the high beam
            }
        }
    }

    public class FightDummiesQuestPart2ElectricBoogaloo : Quest
    {
        public static int dummiesToSpawn = 10;

        public override void Update()
        {
            if (isStarted && !isFinished)
            {
                if (Util.MetricsLogger.sessionStats.dummiesKilled >= dummiesToSpawn+DummyAttackQuest.numDummy)
                {
                    SetIsFinished(true);
                }
            }
        }
    }

    public class CrouchJumpQuest : Quest
    {
        public override void Update()
        {
            if (isStarted && !isFinished)
            {
                //If the trigger is entered
            }
        }
    }

    public class FireballQuest : Quest
    {
        public bool hasUsed = false;

        public override void Update()
        {
            if (isStarted && !isFinished)
            {
                if (hasUsed == true)
                {
                    SetIsFinished(true);
                }
            }
        }
    }

    public class ChainLightningQuest : Quest
    {
        public bool hasUsed = false;

        public override void Update()
        {
            if (isStarted && !isFinished)
            {
                if (hasUsed == true)
                {
                    SetIsFinished(true);
                }
            }
        }
    }

    public class MagicMissileQuest : Quest
    {
        public bool hasUsed = false;

        public override void Update()
        {
            if (isStarted && !isFinished)
            {
                if (hasUsed == true)
                {
                    SetIsFinished(true);
                }
            }
        }
    }

    public class UseSpellQuest : Quest
    {
        public override void Update()
        {
            if(isStarted && !isFinished)
            {

            }
        }
    }
}
