﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUIManager : MonoBehaviour
{
    public List<string> activeQuests;

    public List<GameObject> questItems;

    public GameObject questPrefab;
    public GameObject canvas;

    private void Start()
    {
    }

    public void UpdateUI()
    {
        for(int i = 0; i < questItems.Count; ++i)
        {
            questItems[i].transform.position = Vector3.up * (Screen.height - 32 * i);
        }
    }

    public void AddQuest(string questName)
    {
        activeQuests.Add(questName);
        GameObject newUI = Instantiate(questPrefab, canvas.transform);
        questItems.Add(newUI);
        
        newUI.GetComponentInChildren<Text>().text = questName;
        newUI.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, newUI.GetComponentInChildren<Text>().preferredWidth + 10);
        UpdateUI();
    }

    public void FinishQuest(string questName)
    {
        int index = activeQuests.IndexOf(questName);
        GameObject obj = questItems[index];
        activeQuests.Remove(activeQuests[index]);
        questItems.Remove(obj);
        Destroy(obj);
        UpdateUI();
    }
}
