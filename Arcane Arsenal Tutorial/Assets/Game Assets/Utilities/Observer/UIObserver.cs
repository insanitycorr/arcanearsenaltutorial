﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObserverPattern;

public class UIObserver : Observer
{
    QuestUIManager questUIManager;

    public override void OnNotify(string questName, ObserverPattern.Event theEvent)
    {
        if (questUIManager == null)
            questUIManager = Object.FindObjectOfType<QuestUIManager>();

        switch (questName)
        {
            case "MoveQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Move with WASD");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Move with WASD");
                        break;
                }
                break;
            case "RotateQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Look around with your mouse");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Look around with your mouse");
                        break;
                }
                break;
            case "DummyAttackQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Shoot with Left Click and kill the dummy. Press R to reload");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Shoot with Left Click and kill the dummy. Press R to reload");
                        break;
                }
                break;
            case "SlowDownQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Hold shift to slow down");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Hold shift to slow down");
                        break;
                }
                break;
            case "FightDummiesQuestPart2ElectricBoogaloo":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Kill the dummies. Press on 1-5 to switch between guns.");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Kill the dummies. Press on 1-5 to switch between guns.");
                        break;
                }
                break;
            case "CrouchJumpQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Crouch with control while jumping");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Crouch with control while jumping");
                        break;
                }
                break;
            case "UseSpellQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Please step onto each platform to test each spell");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
						questUIManager.FinishQuest("Please step onto each platform to test each spell");
						questUIManager.AddQuest("CONGRATULATIONS, You finished!!!!");
                        break;
                }
                break;
            case "FireballQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Press P to switch spells and hold E (and release) to cast Fireball");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Press P to switch spells and hold E (and release) to cast Fireball");
                        break;
                }
                break;
            case "ChainLightningQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Press P to switch spells and hold E (and release) to cast Chain Lightning");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Press P to switch spells and hold E (and release) to cast Chain Lightning");
                        break;
                }
                break;
            case "MagicMissileQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        questUIManager.AddQuest("Press P to switch spells and hold E (and release) to cast Magic Missile");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        questUIManager.FinishQuest("Press P to switch spells and hold E (and release) to cast Magic Missile");
                        break;
                }
                break;
        }
    }

    
}
