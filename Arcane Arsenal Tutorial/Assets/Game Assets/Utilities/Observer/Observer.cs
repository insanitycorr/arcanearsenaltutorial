﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObserverPattern
{
    //TODO: Add any events you want to observe here
    public enum Event
    {
        QUEST_STARTED,
        QUEST_ENDED
    }

    public abstract class Observer
    {
        public virtual void OnNotify(string questName, Event theEvent) 
        { 
            
        }
    }

    public abstract class Subject
    {
        protected List<Observer> observers = new List<Observer>();

        public void AddObserver(Observer observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(Observer observer)
        {
            observers.Remove(observer);
        }

        public void Notify(Object theObject, Event theEvent)
        {
            foreach(Observer observer in observers)
            {
                observer.OnNotify(theObject.GetType().Name, theEvent);
            }
        }

        public abstract void Update();
    }

}