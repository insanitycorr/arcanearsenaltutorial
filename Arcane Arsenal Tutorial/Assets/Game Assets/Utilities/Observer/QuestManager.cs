﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quests;

public class QuestManager : MonoBehaviour
{
    public Transform dummySpawnPos1;
    public Transform dummySpawnPos2;

    MoveQuest moveQuest = new MoveQuest();
    RotateQuest rotateQuest = new RotateQuest();
    DummyAttackQuest dummyAttackQuest = new DummyAttackQuest();
    SlowDownQuest slowDownQuest = new SlowDownQuest();
    FightDummiesQuestPart2ElectricBoogaloo dummyAttackPt2Quest = new FightDummiesQuestPart2ElectricBoogaloo();
    CrouchJumpQuest crouchJumpQuest = new CrouchJumpQuest();
    UseSpellQuest useSpellQuest = new UseSpellQuest();
    public FireballQuest fireballQuest = new FireballQuest();
    public ChainLightningQuest chainLightningQuest = new ChainLightningQuest();
    public MagicMissileQuest magicMissileQuest = new MagicMissileQuest();

    QuestObserver questObserver = new QuestObserver();

    // Start is called before the first frame update
    void Start()
    {
        moveQuest.AddObserver(questObserver);
        rotateQuest.AddObserver(questObserver);
        dummyAttackQuest.AddObserver(questObserver);
        slowDownQuest.AddObserver(questObserver);
        dummyAttackPt2Quest.AddObserver(questObserver);
        crouchJumpQuest.AddObserver(questObserver);
        useSpellQuest.AddObserver(questObserver);
        fireballQuest.AddObserver(questObserver);
        chainLightningQuest.AddObserver(questObserver);
        magicMissileQuest.AddObserver(questObserver);

        moveQuest.SetIsStarted(true);
        rotateQuest.SetIsStarted(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (dummyAttackPt2Quest.GetIsFinished())
        {
            crouchJumpQuest.SetIsStarted(true);
        }

        if (useSpellQuest.GetIsStarted() && !useSpellQuest.GetIsFinished())
        {
            if (fireballQuest.GetIsFinished() && chainLightningQuest.GetIsFinished() && magicMissileQuest.GetIsFinished())
            {
                useSpellQuest.SetIsFinished(true);
            }
        }

        //Update all objects
        moveQuest.Update();
        rotateQuest.Update();
        dummyAttackQuest.Update();
        slowDownQuest.Update();
        dummyAttackPt2Quest.Update();
        crouchJumpQuest.Update();
        useSpellQuest.Update();
        fireballQuest.Update();
        chainLightningQuest.Update();
        magicMissileQuest.Update();
    }

    /// OnTriggerEnter is called when the Collider other enters the trigger.
    void OnTriggerEnter(Collider other)
    {
        //Do the trigger stuff to mess with the other quests
        if (other.name == "Trigger1")
        {
            if (!dummyAttackQuest.GetIsStarted())
            {
                GameObject enemy = ObjectPoolManager.GetInstance().GetObject("EnemyPool");
                //Reactivate the head in case a headshot was made previously
                foreach (ShootablePool child in enemy.GetComponentsInChildren<Shootable>(true))
                {
                    child.gameObject.SetActive(true);
                    child.ResetHealth();
                }

                Vector3 pos = dummySpawnPos1.position;
                enemy.transform.position = pos;
                enemy.transform.GetChild(0).transform.position = pos;
            }

            dummyAttackQuest.SetIsStarted(true);
        }
        if (other.name == "Trigger4")
        {
            slowDownQuest.SetIsStarted(true);
        }
        if (other.name == "Trigger5")
        {
			if (slowDownQuest.GetIsStarted() && !slowDownQuest.GetIsFinished())
			{
				slowDownQuest.SetIsFinished(true);
			}

            if (!dummyAttackPt2Quest.GetIsStarted())
            {
                for (int i = 0; i < FightDummiesQuestPart2ElectricBoogaloo.dummiesToSpawn; i++)
                {
                    GameObject enemy = ObjectPoolManager.GetInstance().GetObject("EnemyPool");
                    //Reactivate the head in case a headshot was made previously
                    foreach (ShootablePool child in enemy.GetComponentsInChildren<Shootable>(true))
                    {
                        child.gameObject.SetActive(true);
                        child.ResetHealth();
                    }

                    Vector3 pos = dummySpawnPos2.position;
                    enemy.transform.position = pos;
                    enemy.transform.GetChild(0).transform.position = pos;
                }
            }

            dummyAttackPt2Quest.SetIsStarted(true);
        }
        if (other.name == "Trigger7")
        {
            crouchJumpQuest.SetIsFinished(true);
        }
        if (other.name == "Trigger8")
        {
            useSpellQuest.SetIsStarted(true);
        }
        if (other.name == "Trigger9")
        {
            fireballQuest.SetIsStarted(true);
        }
        if (other.name == "Trigger10")
        {
            chainLightningQuest.SetIsStarted(true);
        }
        if (other.name == "Trigger11")
        {
            magicMissileQuest.SetIsStarted(true);
        }
    }
}

public class QuestObserver : ObserverPattern.Observer
{
    UIObserver uiObserver = new UIObserver();

    public override void OnNotify(string questName, ObserverPattern.Event theEvent)
    {
        uiObserver.OnNotify(questName, theEvent);
        
        switch (questName)
        {
            case "MoveQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("MOVE QUEST STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("MOVE QUEST COMPLETE");
                        break;
                }
                break;
            case "RotateQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("ROTATE QUEST STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("ROTATE QUEST COMPLETE");
                        break;
                }
                break;
            case "DummyAttackQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("DUMMY QUEST 1 STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("DUMMY QUEST 1 COMPLETE");
                        break;
                }
                break;
            case "SlowDownQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("SLOW DOWN QUEST STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("SLOW DOWN QUEST COMPLETE");
                        break;
                }
                break;
            case "FightDummiesQuestPart2ElectricBoogaloo":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("FIGHT DUMMIES 2 QUEST STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("FIGHT DUMMIES 2 QUEST COMPLETE");
                        break;
                }
                break;
            case "CrouchJumpQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("CROUCH JUMP QUEST STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("CROUCH JUMP QUEST COMPLETE");
                        break;
                }
                break;
            case "UseSpellQuest":
                switch(theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                     Debug.Log("USE SPELLS QUEST COMPLETE");
                    break;
                    case ObserverPattern.Event.QUEST_ENDED:
                     Debug.Log("USE SPELLS QUEST COMPLETE");
                    break;
                }
                break;
            case "FireballQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("FIREBALL QUEST STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("FIREBALL QUEST COMPLETE");
                        break;
                }
                break;
            case "ChainLightningQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("CHAIN LIGHTNING QUEST STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("CHAIN LIGHTNING QUEST COMPLETE");
                        break;
                }
                break;
            case "MagicMissileQuest":
                switch (theEvent)
                {
                    case ObserverPattern.Event.QUEST_STARTED:
                        Debug.Log("MAGIC MISSILE QUEST STARTED");
                        break;
                    case ObserverPattern.Event.QUEST_ENDED:
                        Debug.Log("MAGIC MISSILE QUEST COMPLETE");
                        break;
                }
                break;
        }
    }
}




