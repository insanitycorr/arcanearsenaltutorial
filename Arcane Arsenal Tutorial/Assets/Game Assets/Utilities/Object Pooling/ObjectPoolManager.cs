﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectPoolManager
{
    private static ObjectPoolManager instance;

    private List<KeyValuePair<string, Queue<GameObject>>> pools;

    private ObjectPoolManager()
    {
        pools = new List<KeyValuePair<string, Queue<GameObject>>>();
    }

    public void InitPool(GameObject obj, string name, int size)
    {
        Queue<GameObject> pool = GetPool(name);
        for (int i = 0; i < size; ++i)
        {
            GameObject tempObj = MonoBehaviour.Instantiate(obj);
            tempObj.SetActive(false);
            pool.Enqueue(tempObj);
        }
    }

    public Queue<GameObject> GetPool(string name)
    {
        List<KeyValuePair<string, Queue<GameObject>>> foundPools = FindPools(name);

        if (foundPools.Count < 1)
        {
            Queue<GameObject> pool = new Queue<GameObject>();
            pools.Add(new KeyValuePair<string, Queue<GameObject>>(name, pool));
            return pool;
        }
        else return foundPools[0].Value;
    }

    public GameObject GetObject(string name)
    {
        List<KeyValuePair<string, Queue<GameObject>>> foundPools = FindPools(name);

        if (foundPools.Count < 1)
        {
            Debug.LogError("Attempting to take an object from an uninitialized pool");
            return null;
        }
        else if (foundPools[0].Value.Count < 1)
        {
            Debug.LogWarning("Attempting to take an object from an empty pool");
            return null;
        }
        else
        {
            GameObject obj = foundPools[0].Value.Dequeue();
            obj.SetActive(true);
            foreach (Transform child in obj.transform)
                child.gameObject.SetActive(true);
            return obj;
        }
    }

    public void PoolObject(GameObject obj, string name)
    {
        List<KeyValuePair<string, Queue<GameObject>>> foundPools = FindPools(name);

        if (foundPools.Count < 1)
            Debug.LogError("Attempting to pool an object into an uninitialized pool");
        else
        {
            obj.SetActive(false);
            foundPools[0].Value.Enqueue(obj);
        }
    }

    public List<KeyValuePair<string, Queue<GameObject>>> FindPools(string name)
    {
        //This is the best way I could figure out how to convert an IEnumerable to a List
        IEnumerable<KeyValuePair<string, Queue<GameObject>>> foundPools = pools.Where(pool => pool.Key == name);
        List<KeyValuePair<string, Queue<GameObject>>> namedPools = new List<KeyValuePair<string, Queue<GameObject>>>();
        foreach (KeyValuePair<string, Queue<GameObject>> pool in foundPools)
            namedPools.Add(pool);
        return namedPools;
    }

    public static ObjectPoolManager GetInstance()
    {
        if (instance == null)
            instance = new ObjectPoolManager();

        return instance;
    }

}
