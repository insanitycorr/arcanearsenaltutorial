﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using Util;

namespace Util {
    public class MetricsLogger : MonoBehaviour 
    {
        #region DLL IMPORTS
        const string DLL_NAME = "USERMETRICSLOGGER";

        [DllImport (DLL_NAME)]
        public static extern void InitLogger ();


        [DllImport (DLL_NAME)]
        public static extern void SetActiveUser ([MarshalAs(UnmanagedType.LPStr)] string userName);

        //FASTER WAY TO DO IT IS TO UPDATE ONCE AT THE END
        [DllImport (DLL_NAME)]
        public static extern void SetActiveUserStat(Util.StatsCS stat);

        [DllImport (DLL_NAME)]
        public static extern StatsCS GetActiveUserStat();


        //SLOWER WAY TO DO IT IS TO UPDATE THESE EVERY TIME WHEN THEY CHANGE
        [DllImport (DLL_NAME)]
        public static extern void SetTimePlayed (float _timePlayed);

        [DllImport (DLL_NAME)]
        public static extern float GetTimePlayed ();


        [DllImport (DLL_NAME)]
        public static extern void SetDummiesKilled (int _dummiesKilled);

        [DllImport (DLL_NAME)]
        public static extern int GetDummiesKilled ();


        [DllImport (DLL_NAME)]
        public static extern void SetShotsHit (float _shotsHit);

        [DllImport (DLL_NAME)]
        public static extern void SetShotsFired (float _shotsFired);

        [DllImport (DLL_NAME)]
        public static extern float GetShotsHit ();

        [DllImport (DLL_NAME)]
        public static extern float GetShotsFired ();

        [DllImport (DLL_NAME)]
        public static extern float GetAccuracy ();


        [DllImport (DLL_NAME)]
        public static extern void CloseLogger ();

        public static StatsCS sessionStats = new StatsCS();
        public string userName = "SirMouthAlot";

        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        void Start()
        {
            //Inits everything
            InitLogger();

            SetActiveUser(userName);
            sessionStats = GetActiveUserStat();
        }

        /// Update is called every frame, if the MonoBehaviour is enabled.
        void Update()
        {
            //Increment time played
            sessionStats.timePlayed = sessionStats.timePlayed + Time.deltaTime;
        }

        /// This function is called when the MonoBehaviour will be destroyed.
        void OnDestroy()
        {
            SetActiveUserStat(sessionStats);

            CloseLogger();
        }

        #endregion
    }
}
