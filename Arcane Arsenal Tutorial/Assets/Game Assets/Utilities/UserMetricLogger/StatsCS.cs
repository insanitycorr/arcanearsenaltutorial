﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Util
{
    public struct StatsCS
    {
        public void UpdateAccuracy()
        {
            if (shotsFired > 0.0f)
            {
                accuracy = (shotsHit / shotsFired) * 100.0f;
            }
            else
            {
                accuracy = 100.0f;
            }
        }

        //Time Played
        public float timePlayed;

        //Dummies killed
        public int dummiesKilled;

        //Accuracy values
        public float accuracy;
        public float shotsHit;
        public float shotsFired;
    };
}