﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;

public class ShootablePool : Shootable
{
    public override void Die()
    {
        //Increments dummies killed
        MetricsLogger.sessionStats.dummiesKilled++;
        
        gameObject.SetActive(false);
    }
}
