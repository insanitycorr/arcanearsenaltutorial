﻿using UnityEngine;
using Util;

public class Shootable : MonoBehaviour
{

	public float maxHP = 100.0f;

	public float HP;

	private void Start() {
		HP = maxHP;
	}

	public void TakeDamage(float dmgVal)
	{
		//Increment Shots Hit
		MetricsLogger.sessionStats.shotsHit = MetricsLogger.sessionStats.shotsHit + 1.0f;

		HP -= dmgVal;
		if (HP <= 0f)
		{
			Die();
		}
	}

	public void ResetHealth()
	{
		HP = maxHP;
	}

	public virtual void Die()
	{	
		//Increments dummies killed
        MetricsLogger.sessionStats.dummiesKilled++;
		
		Destroy(gameObject);
	}
}
