﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
[RequireComponent(typeof(SphereCollider))]
public class ExplosionLogic : MonoBehaviour
{
    public float explosionRadius;
    public float explosionForce;
    public AnimationCurve damageFalloff;

    private ParticleSystem ps;

    void Awake()
    {
        ps = GetComponent<ParticleSystem>();
        GetComponent<SphereCollider>().radius = explosionRadius / transform.localScale.x;
    }
    
    void Update()
    {
        if(!ps.IsAlive())
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 forceDirection = (other.transform.position - transform.position);
        float mag = forceDirection.magnitude;

        float damage = damageFalloff.Evaluate(mag / explosionRadius);
        Shootable oShootable = other.GetComponent<Shootable>();
        if (oShootable != null)
            oShootable.TakeDamage(damage);

        Rigidbody rb = other.GetComponent<Rigidbody>();
        if (rb != null)
            rb.AddForce(forceDirection.normalized * (explosionRadius - mag) * explosionForce, ForceMode.Impulse);
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, explosionRadius);
    }
}
