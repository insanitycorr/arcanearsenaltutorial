﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Delet this
using UnityEngine.UI;

enum Phase
{
    Charging,
    Firing
}


public class SpellController : MonoBehaviour
{
    #region Temporary
        public Text levelUI;
        public Slider timeUI;
        public Spell[] spells;
        public int index = 0;
	#endregion

	public Transform handPos;
    public Spell activeSpell;

    //How long has the player held the button
    public float chargeTimer;
    public int chargeLevel;

    private Phase phase;


    //TODO: change this after GDW1
    private SpellData sd;
    private GameObject target;

    private void Start()
    {
        if(Camera.main == null)
        {
            Debug.LogError("MAIN CAMERA NOT TAGGED");
        }
    }

    void Update()
    {

        #region Debug

        if(Input.GetKeyDown(KeyCode.P))
        {
            index++;
            index = index % spells.Length;
            activeSpell = spells[index];
            phase = Phase.Charging;
            chargeTimer = 0;
        }

        #endregion


        switch (phase)
        {
            case Phase.Firing:
                if (chargeLevel > 0)
                {
                    chargeTimer += Time.deltaTime;

                    if (chargeTimer >= activeSpell.fireDelay)
                    {
                        //Fire a shot
                        //TODO: Make this not instantiation??
                        GameObject spell = Instantiate(activeSpell.projectile, handPos.position + transform.forward, Camera.main.transform.rotation);
                        switch (activeSpell.name)
                        {
                            case "Fireball":
                                if (GetComponent<QuestManager>().fireballQuest.GetIsStarted() && !GetComponent<QuestManager>().fireballQuest.GetIsFinished())
                                    GetComponent<QuestManager>().fireballQuest.hasUsed = true;
                                break;
                            case "Magic Missile":
                                if (GetComponent<QuestManager>().magicMissileQuest.GetIsStarted() && !GetComponent<QuestManager>().magicMissileQuest.GetIsFinished())
                                    GetComponent<QuestManager>().magicMissileQuest.hasUsed = true;
                                break;
                            case "Chain Lightning":
                                if (GetComponent<QuestManager>().chainLightningQuest.GetIsStarted() && !GetComponent<QuestManager>().chainLightningQuest.GetIsFinished())
                                    GetComponent<QuestManager>().chainLightningQuest.hasUsed = true;
                                break;
                        }


                        chargeTimer = 0;

                        sd = spell.GetComponent<SpellData>();
                        if (sd != null)
                        {
                            if (activeSpell.rayCast)
                                sd.target = target;
                            //if (activeSpell.rayCast)
                            //{
                            //    RaycastHit hit;
                            //    if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
                            //        if (hit.collider.gameObject.GetComponent<Shootable>() != null)
                            //            sd.target = hit.collider.gameObject;
                            //}

                            sd.rateOfChange = activeSpell.rateOfChange;
                            if (activeSpell.behaviour == SpellBehaviour.Damage)
                            {
                                if (sd != null)
                                {
                                    sd.charges = chargeLevel;
                                }
                                chargeLevel = 0;
                            }
                        }
                        chargeLevel--;
                    }
                }
                else
                {
                    chargeTimer = 0;
                    phase = Phase.Charging;
                }
                break;
            default:
                //Charging Phase

                if (activeSpell.chargeRate == 0)
                    chargeLevel = activeSpell.charges;
                else
                    chargeLevel = Mathf.Min((int)Mathf.Floor(chargeTimer / activeSpell.chargeRate), activeSpell.charges);

                //TODO: Change to proper input
                if (Input.GetKey(KeyCode.E))
                {
                    if (chargeTimer < activeSpell.charges * activeSpell.chargeRate)
                        chargeTimer += Time.deltaTime;
                }
                else if (Input.GetKeyUp(KeyCode.E))
                {
                    phase = Phase.Firing;
                    chargeTimer = 0;
                    //TODO: Move this back up to the commented out section
                    if (activeSpell.rayCast)
                    {
                        RaycastHit hit;
                        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
                        {
                            if (hit.collider.gameObject.GetComponent<Shootable>() != null)
                                target = hit.collider.gameObject;
                        }
                        else
                            target = null;
                    }
                }

                //TODO: Delete this lol, or add in UI
                //timeUI.value = (chargeTimer % activeSpell.chargeRate) / activeSpell.chargeRate;
                break;
        }

        //TODO: Replace this
        levelUI.text = activeSpell.name;
    }
}
