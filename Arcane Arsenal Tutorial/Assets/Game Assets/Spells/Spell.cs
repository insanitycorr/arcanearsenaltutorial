﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpellBehaviour
{
    MultiShot,
    Damage
}


[CreateAssetMenu(fileName = "New Spell", menuName = "Spell")]
public class Spell : ScriptableObject
{
    public new string name;

    [Header("Number of levels that the spell can be charged to")]
    public int charges;

    [Header("Time it takes to charge between levels")]
    public float chargeRate;

    [Header("Type of spell I guess?")]
    public SpellBehaviour behaviour;

    [Header("By what amount does the behaviour scale?")]
    public AnimationCurve rateOfChange;

    [Header("How long between shots does the spell take?")]
    public float fireDelay;
    
    [Header("The spell prefab to be summoned")]
    public GameObject projectile;

    [Header("Should the spell raycast and find a target?")]
    public bool rayCast;
}
