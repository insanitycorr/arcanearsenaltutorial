﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ChainLightning : MonoBehaviour
{
    private SpellData spellData;
    private List<GameObject> hits;

	public float force;
    public float speed;
    public float damage;
    public float maxLifetime;

    private float lifetime;

    private void Awake()
    {
        hits = new List<GameObject>();
        spellData = GetComponent<SpellData>();
        lifetime = maxLifetime;
    }

    private void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
        lifetime -= Time.deltaTime;
        if (lifetime <= 0)
            Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        //If the lightning has hit an enemy
        Shootable health = other.GetComponent<Shootable>();
        if (health != null)
        {
            if (hits.Contains(other.gameObject))
            {
                //This object has been hit before!

            }
            else if (spellData.charges > 0)
            {
                hits.Add(other.gameObject);
                transform.position = other.transform.position;
				other.GetComponent<Rigidbody>().AddForce(transform.forward*force);
                health.TakeDamage(damage);
                lifetime = maxLifetime; //Refresh the lifetime of the spell;

                //TODO: Adjust to properly target enemies
                Shootable[] enemies = FindObjectsOfType<Shootable>();
                enemies = enemies.OrderBy(enemy => Vector3.Distance(other.transform.position, enemy.transform.position)).ToArray();

                int index;

                for (index = 1; index < enemies.Length; ++index)
                {
                    if (!hits.Contains(enemies[index].gameObject))
                        break;
                }
                
                if (!(index < enemies.Length))
                    Destroy(gameObject);
                else
                    transform.forward = (enemies[index].transform.position - other.transform.position).normalized;

                --spellData.charges;
            }
            else
                Destroy(gameObject);
        }
        else
            Destroy(gameObject);
    }
}
