﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellData : MonoBehaviour
{
    public GameObject target;
    public int charges;
    public AnimationCurve rateOfChange;
}