﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAfterTime : MonoBehaviour
{
    public float maxLifetime;

    private float lifetime;

    private void Awake()
    {
        lifetime = maxLifetime;
    }

    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0)
            Destroy(gameObject);
    }
}
