﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicMissile : MonoBehaviour
{
    public float speed;

    public GameObject explosion;

    private SpellData spellData;
    private Rigidbody rb;

    private void Start()
    {
        spellData = GetComponent<SpellData>();
        rb = GetComponent<Rigidbody>();

        //spellData.rateOfChange;
    }

    private void Update()
    {
        if(spellData.target != null)
            transform.LookAt(spellData.target.transform);
        
    }

    private void FixedUpdate()
    {
        if (spellData.target != null)
            rb.MovePosition(((spellData.target.transform.position - transform.position).normalized * speed) + transform.position);
        else
            rb.MovePosition(transform.forward * speed + transform.position);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
