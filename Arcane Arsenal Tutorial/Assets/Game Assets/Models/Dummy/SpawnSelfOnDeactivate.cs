﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSelfOnDeactivate : MonoBehaviour
{
    bool isQuitting = false;
    public GameObject toSpawn;

    private void OnApplicationQuit() 
    {
        isQuitting = true;
    }

    private void OnDisable()
    {
        if (!isQuitting)
        {
            GameObject self = Instantiate(toSpawn);
            self.transform.position = transform.position;
            self.transform.rotation = transform.rotation;
            self.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity;
            Destroy(self, 5.0f);
        }
    }
}
