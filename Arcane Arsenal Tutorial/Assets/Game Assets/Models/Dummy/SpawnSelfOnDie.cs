﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSelfOnDie : MonoBehaviour
{
    bool isQuitting = false;
    public GameObject toSpawn;

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

    private void OnDestroy()
    {
        if (!isQuitting)
        {
            GameObject self = Instantiate(toSpawn);
            self.transform.position = transform.position;
            self.transform.rotation = transform.rotation;
            self.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity;
        }
    }
}
