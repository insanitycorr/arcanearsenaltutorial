﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Delet
using UnityEditor;

public class HoverBall : MonoBehaviour
{
    private Rigidbody rb;
    public float hoverHeight;
    public float scale;
    public float rotScale;
    public float speed;

    public bool consious;

    [Header("Feet Setup")]
    public float footDistance;
    public float stepHeight;
    public float stepSpeed;
    public GameObject[] feet;

    //Which foot should move next.
    public int foot;
    private Vector3[] feetOrigin;

    private Ray ray;
    private RaycastHit hit;

    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        feetOrigin = new Vector3[2];
        feetOrigin[0] = feet[0].transform.position - transform.position;
        feetOrigin[1] = feet[1].transform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        hoverHeight = Mathf.Abs(hoverHeight);
        //if(Input.GetKeyDown(KeyCode.Tab))
        //{
        //    transform.position = Vector3.zero + Vector3.up;
        //    rb.velocity = Vector3.zero;
        //}
        //Debug.Log(feetOrigin[0]);
        
        if (transform.childCount < 2)
            consious = false;
    }

    private void FixedUpdate()
    {
        if (consious)
        {
            if (rb.drag != 10)
                rb.drag = 10;

            //rb.AddForce(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * speed);

            //Walk toward origin
            //rb.AddForce(-transform.position.normalized * speed);

            rb.AddForce((target.position - transform.position).normalized * speed);


            ray.origin = transform.position;
            ray.direction = Vector3.down;

            Physics.Raycast(ray, out hit, Mathf.Infinity);

            if (hit.distance != 0)
            {
                //rb.AddForce(Vector3.up * (hoverHeight - hit.distance) / hoverHeight * amount);
                float desiredVel = (hoverHeight - hit.distance) / Time.fixedDeltaTime;
                float desiredAcc = (desiredVel - rb.velocity.y) / Time.fixedDeltaTime;
                float requiredForce = rb.mass * desiredAcc;
                rb.AddForce(Vector3.up * requiredForce * scale, ForceMode.Impulse);
            }
            //else
            //{
            //    Debug.Log("AI Found over void");
            //}
            //Try and stay upright
            {
                //TODO: Make my own
                Vector3 axis = Vector3.Cross(transform.up.normalized, Vector3.up.normalized);
                float theta = Mathf.Asin(axis.magnitude);
                Vector3 w = axis.normalized * theta / Time.fixedDeltaTime;

                Quaternion q = transform.rotation * rb.inertiaTensorRotation;
                Vector3 T = q * Vector3.Scale(rb.inertiaTensor, (Quaternion.Inverse(q) * w));

                rb.AddTorque(T * rotScale, ForceMode.Impulse);
            }
            
            //Compute feet
            {
                Vector3 footPos = feet[foot].transform.position;
                Vector3 targetPos = feetOrigin[foot] + transform.position;
                targetPos.y = transform.position.y - hit.distance;
                Vector3 relativeFootPos = footPos - targetPos;

                Foot currentFoot = feet[foot].GetComponent<Foot>();

                //Debug.Log(feet[0].GetComponent<Foot>().state + " " + feet[1].GetComponent<Foot>().state);
                if (relativeFootPos.magnitude > footDistance && currentFoot.state == FootState.Inactive)
                {
                    currentFoot.Step(targetPos + Vector3.ClampMagnitude(Vector3.ProjectOnPlane(rb.velocity, Vector3.up), footDistance * 2), feet[foot].transform.position, stepSpeed, stepHeight);

                }
                //switch feet
                if (currentFoot.state == FootState.Complete)
                {
                    ++foot;
                    if (foot > 1)
                        foot = 0;

                    feet[foot].GetComponent<Foot>().Restart();
                }
            }
        }
        else
        {
            //Be unconsious
            if (rb.drag != 0)
                rb.drag = 0;
        }
        

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, Vector3.down * hoverHeight);
        Gizmos.DrawSphere(hit.point, 0.1f);

        //Debug.Log(foot);
        //Debug.Log(feetOrigin);
        //if (feetOrigin[foot] != null)
        //{
        //    Handles.color = Color.blue;
        //    Handles.DrawWireArc(feetOrigin[foot] + transform.position, Vector3.up, (transform.forward * -(foot * 2 - 1)), 180, footDistance);
        //    Handles.DrawLine(transform.forward * footDistance + feetOrigin[foot], -transform.forward * footDistance + feet[foot].transform.position);
        //}
        //Gizmos.color = Color.blue;
        //Gizmos.DrawRay(transform.position, transform.forward * 2);
        //if (rb != null)
        //Gizmos.DrawRay(transform.position, rb.velocity.normalized * 2);
    }
}
