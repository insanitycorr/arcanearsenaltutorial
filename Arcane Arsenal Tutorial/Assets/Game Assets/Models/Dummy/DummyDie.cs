﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyDie : MonoBehaviour
{
    int numChildren;

    private void Start()
    {
        numChildren = 0;
        Transform[] children = transform.GetComponentsInChildren<Transform>();
        foreach (Transform child in children)
        {
            if (child.parent == transform && child.gameObject.activeInHierarchy == true)
                ++numChildren;
        }

    }

    void Update()
    {
        int currentChildren = 0;
        Transform[] children = transform.GetComponentsInChildren<Transform>();
        foreach (Transform child in children)
        {
            if (child.parent == transform && child.gameObject.activeInHierarchy == true)
                ++currentChildren;

        }
        if (currentChildren != numChildren)
        {
            ObjectPoolManager.GetInstance().PoolObject(gameObject, "EnemyPool");
        }
    }
}
