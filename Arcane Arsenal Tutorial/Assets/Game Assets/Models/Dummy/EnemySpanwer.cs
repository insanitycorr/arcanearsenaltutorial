﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpanwer : MonoBehaviour
{
    public Vector3[] spawnPoints;
    public GameObject prefab;

    private void Start()
    {
        ObjectPoolManager.GetInstance().InitPool(prefab, "EnemyPool", 20);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.J))
        {
            GameObject enemy = ObjectPoolManager.GetInstance().GetObject("EnemyPool");
            //Reactivate the head in case a headshot was made previously
            foreach(ShootablePool child in enemy.GetComponentsInChildren<Shootable>(true))
            {
                child.gameObject.SetActive(true);
                child.ResetHealth();
            }

            Vector3 pos = spawnPoints[Random.Range(0, spawnPoints.Length)];
            enemy.transform.position = pos;
            enemy.transform.GetChild(0).transform.position = pos;
        }
    }
}
