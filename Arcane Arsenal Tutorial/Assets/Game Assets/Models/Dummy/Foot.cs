﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FootState
{
    Inactive,
    Moving,
    Complete
}


public class Foot : MonoBehaviour
{
    public FootState state = FootState.Inactive;
    public Vector3 target;
    public Vector3 origin;
    public float stepHeight;
    public float percent;
    public float speed;

    // Update is called once per frame
    void Update()
    {
        if(state == FootState.Moving)
            percent += Time.deltaTime * speed;

        if (percent <= 1.0f)
        {
            Vector3 newPos = Vector3.Lerp(origin, target, percent);

            //Step UP, don't just slide
            float tallest = origin.y;
            if (origin.y < target.y)
                tallest = target.y;

            if (percent < 0.5f)
            {
                newPos.y = Mathf.Lerp(origin.y, tallest + stepHeight, percent * 2);
            }
            else
            {
                newPos.y = Mathf.Lerp(tallest + stepHeight, target.y, percent * 2 - 1);
            }

            transform.position = newPos;
        }
        else
            state = FootState.Complete;
    }

    public void Step(Vector3 to, Vector3 from, float _speed, float _height)
    {
        target = to;
        origin = from;
        speed = _speed;
        stepHeight = _height;
        percent = 0;
        state = FootState.Moving;
    }

    public void Restart()
    {
        percent = 0;
        state = FootState.Inactive;
        origin = transform.position;
    }
}
