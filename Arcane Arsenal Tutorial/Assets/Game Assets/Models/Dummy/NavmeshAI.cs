﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavmeshAI : MonoBehaviour
{
    private NavMeshAgent agent;
    public Transform player;
    public GameObject body;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        //Changed so headless dummies can still attack you
        //if (body.activeInHierarchy == true)
        //{
            if (Vector3.Distance(transform.position, body.transform.position) > 2)
                agent.SetDestination(body.transform.position);
            else
                agent.SetDestination(player.position);
        //}
    }
}
